#version 330 core
out vec4 FragColor;
in vec3 localPos;
in vec2 groundTexCoords;
in vec3 groundColour;

uniform sampler2D equirectangularMap;
uniform sampler2D groundTex;
uniform bool isGround;

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

void main()
{	
	if (isGround)
	{
		FragColor = texture2D(groundTex, groundTexCoords) * vec4(groundColour, 1);
		//FragColor = vec4(1, 0, 1, 1);
		//FragColor = vec4(groundTexCoords.x, groundTexCoords.y, 0, 1);
	}
	else
	{
		vec2 uv = SampleSphericalMap(normalize(localPos));
		vec3 color = texture(equirectangularMap, uv).rgb;
    
		FragColor = vec4(color, 1.0);
	}
}