#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoords;

out vec3 localPos;
out vec2 groundTexCoords;
out vec3 groundColour;

uniform mat4 projection;
uniform mat4 view;
uniform vec2 groundTexScale;
uniform vec3 groundColourTint;

void main()
{
	groundTexCoords = aTexCoords * groundTexScale;
	groundColour = groundColourTint;

    localPos = aPos;  
    gl_Position =  projection * view * vec4(localPos, 1.0);
}