#version 330 core

out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D screenTexture;
uniform sampler2D gaussianBlurTexture;
uniform float bloomIntensity;
uniform float bloomContrast;
uniform float bloomRamp;
uniform vec3 bloomColour;

float exposure = 0.5;
float gamma = 2.2;

vec3 ACESExposure(vec3 x)
{
    float a = 2.51f;
    float b = 0.03f;
    float c = 2.43f;
    float d = 0.59f;
    float e = 0.14f;
    return (x*(a*x+b))/(x*(c*x+d)+e);
}

vec4 bloom(vec4 colour)
{
	vec4 gaus = texture(gaussianBlurTexture, TexCoords);
	vec3 brightCol = ((gaus.rgb - 0.5f) * max(bloomContrast, 0)) + 0.5f;
	float brightFactor = (brightCol.r + brightCol.g + brightCol.b) / 3.0;
	//float brightFactor = (0.2126 * pow(brightCol.r, gamma)) + (0.7512 * pow(brightCol.g, gamma)) + (0.0722 * pow(brightCol.b, gamma));
	brightFactor = pow(brightFactor, bloomRamp);
	brightFactor *= bloomIntensity;
	brightFactor = max(0, brightFactor);

	vec3 bloomCol = colour.rgb + (gaus.rgb * brightFactor * bloomColour);
	return vec4(bloomCol, colour.a);
}


void main()
{ 
	vec4 hdrCol = texture(screenTexture, TexCoords);
	hdrCol = bloom(hdrCol);
	vec4 toneMapped = vec4(ACESExposure(hdrCol.xyz), 1);
	toneMapped = pow(toneMapped, vec4(1.0/gamma));
    FragColor = toneMapped;
}