#version 330 core

out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D image;

uniform float blurSize = 1;
uniform bool horizontal;
//uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);
uniform float weight[11] = float[] (
		0.132429,	0.125337,	0.106259,	0.080693,	0.054891,	0.033446,	0.018255,	0.008925,	0.003908,	0.001533,	0.000539

);

vec4 gaussian()
{
	float weightMult = 1;
	vec2 tex_offset = blurSize * 1.0 / textureSize(image, 0); // gets size of single texel
    vec3 result = texture(image, TexCoords).rgb * weight[0] * weightMult; // current fragment's contribution
    if(horizontal)
    {
        for(int i = 1; i < 11; ++i)
        {
            result += texture(image, TexCoords + vec2(tex_offset.x * i, 0.0)).rgb * weight[i] * weightMult;
            result += texture(image, TexCoords - vec2(tex_offset.x * i, 0.0)).rgb * weight[i] * weightMult;
        }
    }
    else
    {
        for(int i = 1; i < 11; ++i)
        {
            result += texture(image, TexCoords + vec2(0.0, tex_offset.y * i)).rgb * weight[i] * weightMult;
            result += texture(image, TexCoords - vec2(0.0, tex_offset.y * i)).rgb * weight[i] * weightMult;
        }
    }
    return vec4(result * 1, 1.0);
}

void main()
{ 
    FragColor = gaussian();
}