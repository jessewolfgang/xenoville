#version 330

in vec3 Texcoord;

out vec4 outColor;

uniform samplerCube skyboxCube;
uniform vec3 colourTint;
uniform float exposure = 1;

void main()
{	
	vec3 baseColour = exposure * colourTint * texture(skyboxCube, Texcoord).rgb;

	//baseColour = baseColour / (baseColour + vec3(1.0));
    //baseColour = pow(baseColour, vec3(1.0/2.2));

	//baseColour = vec4(0.1, 0.9, 0.3, 1);
	outColor = vec4(baseColour, 1);
}

