#version 330

#define MAX_LIGHTS_POINT 8
#define MAX_LIGHTS_DIRECTIONAL 3

uniform struct PointLight
{
	vec3 position;
	vec4 colour;
	float intensity;
	vec3 ambientPower;
	vec3 specularPower;
};

uniform struct DirectionalLight
{
	vec3 direction;
	vec4 colour;
	float intensity;
	vec3 ambientPower;
	vec3 specularPower;
};

in vec4 Colour;
in vec2 Texcoord;
in vec3 Tangent;
in vec2 Texcoord2;

in vec3 fragNormalView;
in vec3 fragNormalWorld;
in vec3 fragPositionView;
in vec3 fragPositionWorld;
in vec3 fragTintColour;
in vec3 fragViewPosWorld;
in mat3 TBN;
in mat4 modelMat;

out vec4 outColor;

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D emissionMap;
uniform samplerCube reflectionSampler;
uniform sampler2D normalMap2;
uniform float alpha;
uniform float shininess;
uniform vec3 specularMuliplier;
uniform float normalMapMultiplier;
uniform float normalMapMultiplier2;
uniform vec3 emission;
uniform vec3 reflectionIntensity;
uniform float fresnelExponent;
uniform float cutoutThreshold;
uniform bool hasNormal2;

uniform PointLight pointLights[MAX_LIGHTS_POINT];
uniform DirectionalLight directionalLights[MAX_LIGHTS_DIRECTIONAL];

// https://learnopengl.com/Lighting/Multiple-lights
vec3 RenderPointLight(PointLight light, vec3 normal, vec3 fragPosWorld, vec3 viewDir)
{
	vec3 lightDisplacement = light.position - fragPosWorld;
	float lightDistance = length(lightDisplacement);
	vec3 lightDir = normalize(lightDisplacement);

    // diffuse shading
    float diffShading = max(dot(normal, lightDir), 0.0);

    // specular shading
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    float specShading = pow(max(dot(normal, halfwayDir), 0.0), shininess);


    float attenuation = light.intensity * light.intensity / (lightDistance);

    // combine results
    vec3 ambient  = light.ambientPower * fragTintColour  * vec3(texture(diffuseMap, Texcoord));
    vec3 diffuse  = light.colour.rgb * Colour.rgb * fragTintColour * diffShading * vec3(texture(diffuseMap, Texcoord));
    vec3 specular = light.colour.rgb * specularMuliplier * light.specularPower * specShading * vec3(texture(specularMap, Texcoord));
    //ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);

}

vec3 RenderDirectionLight(DirectionalLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(light.direction);

    // diffuse shading
    float diffShading = max(dot(normal, lightDir), 0.0);

    // specular shading
	vec3 halfwayDir = normalize(lightDir + viewDir);  
    float specShading = pow(max(dot(normal, halfwayDir), 0.0), shininess);

	float attenuation = light.intensity;

    // combine results
    vec3 ambient  = light.ambientPower * fragTintColour * vec3(texture(diffuseMap, Texcoord));
    vec3 diffuse  = light.colour.rgb * Colour.rgb * fragTintColour  * diffShading * vec3(texture(diffuseMap, Texcoord));
    vec3 specular = light.colour.rgb * specularMuliplier * light.specularPower * specShading * vec3(texture(specularMap, Texcoord));

	diffuse  *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse + specular);
}

vec4 reflection(vec3 normal, vec3 viewDir)
{
    vec3 R = normalize(reflect(viewDir, normal));
	
	vec4 refCol = texture(reflectionSampler, R) * vec4(reflectionIntensity, 1) * texture(specularMap, Texcoord);
	refCol = max(vec4(0), refCol);
	return refCol;
}

float fresnel(vec3 normal, vec3 viewDir)
{
	float cosTheta = dot(viewDir, normal);
	cosTheta = -abs(cosTheta);
	float normedTheta = ((cosTheta + 1) * 0.5);
	return clamp(pow(normedTheta, fresnelExponent), 0, 1);
}

void main()
{	
	float alphaComponent = texture(diffuseMap, Texcoord).a * alpha;
	if (alphaComponent < cutoutThreshold)
	{
		discard;
	}

	// Using help from https://learnopengl.com/Lighting/Basic-Lighting.
	// The example uses world space to do lighting. Dunno why.

	// Normal map 1.
	vec3 normTex = texture2D(normalMap, Texcoord).xyz;
	normTex = normalize(normTex * 2.0 - 1.0); 
	normTex.x *= normalMapMultiplier;
	normTex.y *= normalMapMultiplier;

	// Normal map 2.
	if (hasNormal2)
	{
		vec3 normTex2 = texture2D(normalMap2, Texcoord2).xyz;
		normTex2 = normalize(normTex2 * 2.0 - 1.0); 
		normTex2.x *= normalMapMultiplier2;
		normTex2.y *= normalMapMultiplier2;

		normTex = mix(normTex, normTex2, 0.5);
		//normTex.x += normTex2.x;
		//normTex.y += normTex2.y;
	}

	vec3 viewNormal = normalize(normTex);
	vec3 worldNormal = normalize(TBN * viewNormal);

    vec3 viewDir = normalize(fragViewPosWorld - fragPositionWorld);
	vec3 viewDirInverse = normalize(fragPositionWorld - fragViewPosWorld);

	vec3 totalLight = vec3(0, 0, 0);

	for (int i = 0; i < MAX_LIGHTS_POINT; i++)
	{
		totalLight += RenderPointLight(pointLights[i], worldNormal, fragPositionWorld, viewDir);
	}

	for (int i = 0; i < MAX_LIGHTS_DIRECTIONAL; i++)
	{
		totalLight += RenderDirectionLight(directionalLights[i], worldNormal, viewDir);
	}

	vec4 reflections = reflection(worldNormal, viewDirInverse);
	float fresnelFac = fresnel(worldNormal, viewDirInverse);

	vec3 emissionCol = emission * texture2D(emissionMap, Texcoord).xyz;

	vec4 col = vec4(totalLight + emissionCol, alphaComponent);
	col = mix(col, reflections, fresnelFac);

	//col = reflections; // Visualise reflections.
	//col = vec4(vec3(fresnelFac), 1); // Visualise fresnel factor.

	outColor = col;
}

