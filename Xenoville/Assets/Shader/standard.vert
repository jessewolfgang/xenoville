#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 colour;
layout (location = 3) in vec2 texcoord;
layout (location = 4) in vec3 tangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 viewPos;
uniform vec2 texScale;
uniform vec2 texOffset;
uniform vec2 texScale2;
uniform vec2 texOffset2;
uniform vec3 tintColour;


out vec4 Colour;
out vec2 Texcoord;
out vec3 Tangent;
out vec2 Texcoord2;
out vec3 fragNormalView;
out vec3 fragNormalWorld;
out vec3 fragPositionView;
out vec3 fragPositionWorld;
out vec3 fragTintColour;
out vec3 fragViewPosWorld;
out mat3 TBN;
out mat4 modelMat;

void main()
{
	// Pass through the texture and colour.
	Texcoord = (texcoord * texScale) + texOffset;
	Texcoord2 = (texcoord * texScale2) + texOffset2;
	Colour = colour;
	Tangent = tangent;
	
	// Send the view space normals for later.
	vec4 normWorld = model * vec4(normal, 0);
	vec4 normView = view * normWorld;
	
	// Make sure the normals are normalised (don't trust the user).
	// Also note that it is better to check this in the vertex shader 
	// as it requires fewer total operations.
	fragNormalWorld =  normalize(normWorld.xyz);
	fragNormalView = normalize(normView.xyz);
	
	// World space vertex position.
	fragPositionWorld = vec3(model * vec4(position, 1));

	// Send the view space positions for later.
	fragPositionView = vec3(view * vec4(fragPositionWorld, 1));
	
	fragTintColour = tintColour;

	fragViewPosWorld = viewPos;

	// Send projected position so we know where to draw on screen.
    gl_Position = proj * vec4(fragPositionView, 1);

	vec3 T = normalize(vec3(model * vec4(Tangent, 0.0)));
	vec3 N = normalize(vec3(model * vec4(normal, 0.0)));
	T = normalize(T - dot(T, N) * N);
	vec3 B = -cross(N, T); // I reckon negative bitangent looks better.
	TBN = mat3(T, B, N);

	modelMat = model;

    // Remember, everything which is passed through to the fragment shader
    // is interpolated between vertices. This includes Texcoord, Colour,
    // fragNormalView, fragPositionView, and gl_Position.
}

