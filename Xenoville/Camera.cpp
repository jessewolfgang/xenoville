#include "Camera.h"
#include "Engine.h"

Camera::Camera() : Object()
{
	setViewTarget(0.0f, 0.0f, 0.0f);

	lockViewTarget = false;
	fov = 45;
	recalculateAspectRatio();
	nearClipPlane = 0.1f;
	farClipPlane = 3000.0f;

	//setRotation(0, 30, -40);
}

Camera::~Camera()
{
}

glm::vec3 Camera::getViewTarget()
{
	return viewTarget;
}

void Camera::setViewTarget(glm::vec3 tar)
{
	this->viewTarget = tar;
}

void Camera::setViewTarget(float x, float y, float z)
{
	setViewTarget(glm::vec3(x, y, z));
}

void Camera::recalculateAspectRatio()
{
	aspectRatio = (float)Engine::WINDOW_WIDTH / (float)Engine::WINDOW_HEIGHT;
}

void Camera::update()
{
	if (lockViewTarget)
	{
		setRotation(lookAt(viewTarget));
	}
	
	glm::vec3 pos = getPosition();
	viewMat = glm::lookAt(pos, pos + forward(), up());

	projMat = glm::perspective(fov, aspectRatio, nearClipPlane, farClipPlane);
}

glm::mat4 Camera::getViewMatrix()
{
	return viewMat;
}

glm::mat4 Camera::getProjectionMatrix()
{
	return projMat;
}
