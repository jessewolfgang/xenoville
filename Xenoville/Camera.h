#pragma once

#include "Object.h"

/// <summary>
/// Represents the camera object. It produces the view and projection matricies.
/// </summary>
/// <seealso cref="Object" />
class Camera : public Object
{
public:
		
	/// <summary>
	/// Whether to lock the camera to the view target.
	/// </summary>
	bool lockViewTarget;

	/// <summary>
	/// The vertical field of view.
	/// </summary>
	float fov;
	
	/// <summary>
	/// The aspect ratio.
	/// </summary>
	float aspectRatio;
	
	/// <summary>
	/// The near clipping plane.
	/// </summary>
	float nearClipPlane;
		
	/// <summary>
	/// The far clipping plane.
	/// </summary>
	float farClipPlane;

	Camera();
	~Camera();
	
	/// <summary>
	/// Gets the view target.
	/// </summary>
	/// <returns></returns>
	glm::vec3 getViewTarget();
	
	/// <summary>
	/// Sets the view target.
	/// </summary>
	/// <param name="tar">The target.</param>
	void setViewTarget(glm::vec3 tar);
	
	/// <summary>
	/// Sets the view target.
	/// </summary>
	/// <param name="x">The x position.</param>
	/// <param name="y">The y position.</param>
	/// <param name="z">The z position.</param>
	void setViewTarget(float x, float y, float z);
	
	/// <summary>
	/// Recalculates the aspect ratio.
	/// </summary>
	void recalculateAspectRatio();
		
	/// <summary>
	/// Refreshes the camera.
	/// </summary>
	void update();
	
	/// <summary>
	/// Gets the view matrix.
	/// </summary>
	/// <returns>A mat4 containing the view matrix.</returns>
	glm::mat4 getViewMatrix();
	
	/// <summary>
	/// Gets the projection matrix.
	/// </summary>
	/// <returns>A mat4 containing the projection matrix.</returns>
	glm::mat4 getProjectionMatrix();


protected:
	glm::vec3 viewTarget;

	glm::mat4 viewMat;
	glm::mat4 projMat;

	
};