#pragma once
#include <GL/glew.h>
#include <glm.hpp>
#include "TextureAsset.h"

/// <summary>
/// Helper class for rendering cube maps.
/// Much of the implementation is from https://learnopengl.com/PBR/IBL/Diffuse-irradiance.
/// </summary>
class Cubemap
{
public:
	
	/// <summary>
	/// Renders an equirectangular texture into a cubemap.
	/// </summary>
	/// <param name="equirectangular">The equirectangular texture.</param>
	/// <param name="resX">X resolution.</param>
	/// <param name="resY">Y resolution.</param>
	/// <param name="groundPlaneTexture">The texture for the plane to include in the render.</param>
	/// <param name="groundTexScale">The ground tex scale.</param>
	/// <param name="groundTint">The ground tint colour.</param>
	/// <returns> A GL cubemap.</returns>
	static GLuint render(TextureAsset* equirectangular, float resX, float resY, TextureAsset* groundPlaneTexture, glm::vec2 groundTexScale, glm::vec3 groundTint);

	/// <summary>
	/// Renders an equirectangular texture into a cubemap.
	/// </summary>
	/// <param name="equirectangular">The equirectangular texture.</param>
	/// <param name="resX">X resolution.</param>
	/// <param name="resY">Y resolution.</param>
	/// <returns>A GL cubemap.</returns>
	static GLuint render(TextureAsset* equirectangular, float resX, float resY);

	/// <summary>
	/// Renders a cube.
	/// Source from https://learnopengl.com/code_viewer_gh.php?code=src/6.pbr/2.1.1.ibl_irradiance_conversion/ibl_irradiance_conversion.cpp.
	/// </summary>
	static void drawCube();
	
	/// <summary>
	/// Draws a ground plane.
	/// </summary>
	/// <param name="yPos">The y position.</param>
	/// <param name="scale">The scale.</param>
	static void drawGroundPlane(float yPos, float scale);

private:
	static GLuint unitCubeVAO;
	static GLuint unitCubeVBO;

	static GLuint groundPlaneVAO;
	static GLuint groundPlaneVBO;
};

