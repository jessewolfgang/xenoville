#include "Engine.h"
#include "TextureAsset.h"
#include "Object.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

int Engine::WINDOW_WIDTH = 1920;
int Engine::WINDOW_HEIGHT = 1080;

// Initialise the static fields.
GLFWwindow* Engine::window = nullptr;
GLuint Engine::frameBuffer;
GLuint Engine::frameBufferColour;
GLuint Engine::depthRenderBuffer;
GLuint Engine::viewportQuadVAO;
GLuint Engine::loadingQuadVAO;
Game* Engine::game = nullptr;
Camera* Engine::camera = nullptr;
double Engine::totalTime = 0;
double Engine::frameDeltaTime = 0;
glm::vec2 Engine::mousePosition = glm::vec2(0, 0);
glm::vec2 Engine::previousMousePosition = glm::vec2(0, 0);
glm::vec2 Engine::mouseVelocity = glm::vec2(0, 0);
bool Engine::mouseIsCaptured = true;
GLuint Engine::shaderStandard;
GLuint Engine::shaderPBR;
GLuint Engine::shaderSkybox;
GLuint Engine::shaderFramebuffer;
GLuint Engine::shaderGaussian;
GLuint Engine::shaderText;
GLuint Engine::shaderCubemap;
TextureAsset* Engine::textureEmptyWhite = nullptr;
TextureAsset* Engine::textureEmptyBlack = nullptr;
TextureAsset* Engine::textureEmptyNormal = nullptr;
std::vector<TextureAsset*> Engine::loadedTextures;
std::vector<MeshAsset*> Engine::loadedMeshes;
std::vector<ModelObject*> Engine::modelObjects;
std::vector<Light*> Engine::lightObjects;
PostProcessing* Engine::postProcessing = nullptr;
Skybox* Engine::skybox = nullptr;
GaussianBlur* Engine::gaus = nullptr;

void Engine::error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

void Engine::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
	{
		mouseIsCaptured = !mouseIsCaptured;
		glfwSetInputMode(window, GLFW_CURSOR, mouseIsCaptured ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
	}

	if (action == GLFW_PRESS)
		game->onKeyDown(key);
}

void Engine::resize_callback(GLFWwindow* window, int width, int height)
{
	Engine::WINDOW_WIDTH = width;
	Engine::WINDOW_HEIGHT = height;

	delete gaus;
	gaus = new GaussianBlur(WINDOW_WIDTH, WINDOW_HEIGHT);
	gaus->blurSize = 30;
	gaus->blurCount = 4;
	createFrameBuffer();
	camera->recalculateAspectRatio();
}

bool Engine::getShaderCompileStatus(GLuint shader)
{
	//Get status
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_TRUE) 
	{
		return true;
	}
	else 
	{
		//Get log
		char buffer[512];
		glGetShaderInfoLog(shader, 512, NULL, buffer);
		std::cout << buffer << std::endl;
		return false;
	}
}

GLuint Engine::compileShader(std::string shaderAssetName)
{
	std::string shaderVertexPath = getAssetPath(shaderAssetName, "Shader") + ".vert";
	std::string shaderFragmentPath = getAssetPath(shaderAssetName, "Shader") + ".frag";

	// Load and compile the Vertex shader.
	std::ifstream in(shaderVertexPath);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	const char* vertSource = contents.c_str();
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertSource, NULL);
	glCompileShader(vertexShader);
	getShaderCompileStatus(vertexShader);

	// Load and compile the Fragment shader.
	std::ifstream in2(shaderFragmentPath);
	std::string contents2((std::istreambuf_iterator<char>(in2)), std::istreambuf_iterator<char>());
	const char* fragSource = contents2.c_str();
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragSource, NULL);
	glCompileShader(fragmentShader);
	getShaderCompileStatus(fragmentShader);

	// Link the shader parts into a shader program.
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);

	return shaderProgram;
}

void Engine::createFrameBuffer()
{
	if (frameBuffer > 0)
	{
		glDeleteRenderbuffers(1, &depthRenderBuffer);
		glDeleteFramebuffers(1, &frameBuffer);
		glDeleteTextures(1, &frameBufferColour);
	}

	// Create our framebuffer.
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &frameBufferColour);
	glBindTexture(GL_TEXTURE_2D, frameBufferColour);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, WINDOW_WIDTH, WINDOW_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferColour, 0);

	glGenRenderbuffers(1, &depthRenderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, WINDOW_WIDTH, WINDOW_HEIGHT);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Unable to create framebuffer.";
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

std::string Engine::getAssetPath(std::string assetName, std::string type)
{
	return "Assets/" + type + "/" + assetName;
}

MeshAsset* Engine::loadMesh(std::string assetName)
{
	// Return the mesh from the mesh pool if possible.
	for (size_t i = 0; i < Engine::loadedMeshes.size(); i++)
	{
		if (Engine::loadedMeshes[i]->assetName == assetName)
		{
			return Engine::loadedMeshes[i];
		}
	}

	Assimp::Importer importer;
	std::string sPath = getAssetPath(assetName, "Mesh") + ".obj";
	importer.ReadFile(sPath, aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_Triangulate | aiProcess_CalcTangentSpace);
	const aiScene* scene = importer.GetScene();

	MeshAsset* meshAsset = new MeshAsset();
	meshAsset->assetName = assetName;
	meshAsset->parts = std::vector<MeshPart*>();

	std::cout << "Loading Mesh: " << sPath << std::endl;

	if (scene)
	{
		if (scene->HasMeshes())
		{
			for (unsigned int i = 0; i < scene->mNumMeshes; i++)
			{
				const struct aiMesh* mesh = scene->mMeshes[i];
				MeshPart* part = new MeshPart();
				part->meshPartName = mesh->mName.C_Str();

				std::cout << "    Part " << i << ":\t" << part->meshPartName << std::endl;
				//part->materialIndex = mesh->mMaterialIndex;
				//part->materialIndex = i;
				part->vertices = std::vector<Vertex>();
				part->vertexCount = 0;

				for (unsigned int t = 0; t < mesh->mNumFaces; ++t)
				{
					const struct aiFace* face = &mesh->mFaces[t];
					part->vertexCount += face->mNumIndices;

					if (face->mNumIndices != 3)
					{
						//std::cout << "WARNING " << __FILE__ << " : " << __LINE__ << " - faces are not triangulated" << std::endl;
					}

					for (unsigned int j = 0; j < face->mNumIndices; j++)
					{
						Vertex* vert = new Vertex();
						int index = face->mIndices[j];

						// Vertex positions
						vert->position = glm::vec3(mesh->mVertices[index].x, mesh->mVertices[index].y, mesh->mVertices[index].z);

						// Vertex normals
						if (mesh->mNormals != NULL)
						{
							vert->normal = glm::vec3(mesh->mNormals[index].x, mesh->mNormals[index].y, mesh->mNormals[index].z);
						}
						else
						{
							// Who even needs normals?
							vert->normal = glm::vec3(0, 0, 0);
							std::cout << "WARNING: No normals loaded for mesh " << assetName << std::endl;
						}

						// Vertex colours
						if (mesh->mColors[0] != NULL)
						{
							vert->colour = glm::vec4(mesh->mColors[index]->r, mesh->mColors[index]->g, mesh->mColors[index]->b, 1);
						}
						else
						{
							vert->colour = glm::vec4(1, 1, 1, 1);
						}

						// Texture coords
						if (mesh->mTextureCoords[0] != NULL)
						{
							vert->texCoords = glm::vec2(mesh->mTextureCoords[0][index].x, 1 - mesh->mTextureCoords[0][index].y);
						}
						else
						{
							vert->texCoords = glm::vec2(0, 0);
						}

						// Tangents
						if (mesh->mTangents != NULL)
						{
							vert->tangent = glm::vec3(mesh->mTangents[index].x, mesh->mTangents[index].y, mesh->mTangents[index].z);
						}
						else
						{
							vert->tangent = glm::vec3(0, 0, 0);
						}

						part->vertices.push_back(*vert);
					}
				}
				meshAsset->parts.push_back(part);
			}
		}
	}
	else
	{
		std::cout << "No object found! - Looking for " << assetName << std::endl;
	}

	loadedMeshes.push_back(meshAsset);
	return meshAsset;
}

GLuint Engine::getShaderStandard()
{
	return shaderStandard;
}

GLuint Engine::getShaderPBR()
{
	return shaderPBR;
}

GLuint Engine::getShaderSkybox()
{
	return shaderSkybox;
}

GLuint Engine::getShaderGaussian()
{
	return shaderGaussian;
}

GLuint Engine::getShaderCubemap()
{
	return shaderCubemap;
}

Camera * Engine::getCamera()
{
	return camera;
}

double Engine::deltaTime()
{
	return frameDeltaTime;
}

TextureAsset* Engine::loadTexture(std::string assetName)
{
	return loadTexture(assetName, Engine::Dunno, "png");
}

TextureAsset* Engine::loadTexture(std::string assetName, TextureType type)
{
	return loadTexture(assetName, type, "png");
}

TextureAsset* Engine::loadTexture(std::string assetName, TextureType type, std::string extension)
{
	// Return the texture from the texture pool if possible.
	for (size_t i = 0; i < Engine::loadedTextures.size(); i++)
	{
		if (Engine::loadedTextures[i]->assetName == assetName)
		{
			return Engine::loadedTextures[i];
		}
	}

	// There was no texture in the pool. Load one, add it to the pool, then return it.
	TextureAsset* newTex = new TextureAsset();
	newTex->assetName = assetName;

	std::string sPath = getAssetPath(assetName, "Texture") + "." + extension;
	const char* cPath = sPath.c_str(); // Parameter requires C-style string.

	std::cout << "Loading Texture: " << sPath << std::endl;

	if (type == HDR)
	{
		stbi_set_flip_vertically_on_load(true);
		int width = 0;
		int height = 0;
		int channels = 0;

		float *data = stbi_loadf(cPath, &width, &height, &channels, 4);

		if (!data)
		{
			std::cout << "Unable to load HDR texture " << cPath << std::endl;
			return nullptr;
		}

		glGenTextures(1, &newTex->glTextureId);

		glBindTexture(GL_TEXTURE_2D, newTex->glTextureId);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGBA, GL_FLOAT, data);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		int width = 0;
		int height = 0;
		int channels = 0;

		stbi_set_flip_vertically_on_load(false);
		unsigned char* image = stbi_load(cPath, &width, &height, &channels, 4);

		if (!image || image == nullptr)
		{
			std::cout << "Unable to load texture " << cPath << std::endl;
			return nullptr;
		}

		glGenTextures(1, &newTex->glTextureId);

		glBindTexture(GL_TEXTURE_2D, newTex->glTextureId);

		// Originally uses GL_RGBA.
		GLint sourceFormat = GL_RGBA;
		if (type == Albedo)
		{
			sourceFormat = GL_SRGB8_ALPHA8;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, sourceFormat, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);

		//Set sampler parameters.(Repeat, Linear)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		stbi_image_free(image);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	loadedTextures.push_back(newTex);

	return newTex;
}

TextureAsset* Engine::createTexture(std::string assetName, TextureType type, glm::vec4 fromColour)
{
	// Return the texture from the texture pool if possible.
	for (size_t i = 0; i < Engine::loadedTextures.size(); i++)
	{
		if (Engine::loadedTextures[i]->assetName == assetName)
		{
			return Engine::loadedTextures[i];
		}
	}

	// There was no texture in the pool. Create one, add it to the pool, then return it.
	TextureAsset* newTex = new TextureAsset();

	glGenTextures(1, &newTex->glTextureId);
	newTex->assetName = assetName;

	glBindTexture(GL_TEXTURE_2D, newTex->glTextureId);

	int width = 1;
	int height = 1;

	float image[] =
	{
		fromColour.r, fromColour.g, fromColour.b, fromColour.a,
	};

	GLint sourceFormat = GL_RGBA;
	if (type == Albedo)
	{
		sourceFormat = GL_SRGB_ALPHA;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, sourceFormat, width, height, 0, GL_RGBA, GL_FLOAT, image);

	//Set sampler parameters. (Clamped, NearestPixel)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	loadedTextures.push_back(newTex);

	return newTex;
}

GLuint Engine::createQuad(glm::vec2 min, glm::vec2 max, glm::vec2 texMin, glm::vec2 texMax)
{
	// Create the viewport quad.
	float quadVertices[] = {
		// positions   // texCoords
		/*-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f*/

		min.x, max.y,  texMin.x, texMax.y,
		min.x, min.y,  texMin.x, texMin.y,
		max.x, min.y,  texMax.x, texMin.y,

		min.x, max.y,  texMin.x, texMax.y,
		max.x, min.y,  texMax.x, texMin.y,
		max.x, max.y,  texMax.x, texMax.y
	};

	GLuint output;

	glGenVertexArrays(1, &output);
	glBindVertexArray(output);
	GLuint quadVBO;
	glGenBuffers(1, &quadVBO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)(2 * sizeof(float))); // Offset is 2 floats from the start.
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);

	return output;
}

double Engine::time()
{
	return totalTime;
}

void Engine::registerModelObject(ModelObject* object)
{
	modelObjects.push_back(object);
}

void Engine::registerLight(Light* light)
{
	lightObjects.push_back(light);
}

std::vector<Light*> Engine::getLights()
{
	return std::vector<Light*>();
}

std::vector<Light*> Engine::getLightsOfType(int max, Light::LightType type)
{
	std::vector<Light*> selected;
	int count = 0;
	for (int i = 0; i < lightObjects.size(); i++)
	{
		Light* l = lightObjects[i];
		if (l->type == type)
		{
			selected.push_back(l);
			count++;
		}

		if (count >= max)
			break;
	}

	return selected;
}

int Engine::getKey(int key)
{
	return glfwGetKey(Engine::window, key);
}

glm::vec2 Engine::getMousePosition()
{
	return mousePosition;
}

glm::vec2 Engine::getMouseVelocity()
{
	return mouseVelocity;
}

void Engine::generateDefaultEngineObjects()
{
	// Compile our shaders.
	shaderFramebuffer = compileShader("framebuffer");
	glUseProgram(shaderFramebuffer);
	glUniform1i(glGetUniformLocation(shaderFramebuffer, "screenTexture"), 0);
	glUniform1i(glGetUniformLocation(shaderFramebuffer, "gaussianBlurTexture"), 1);

	shaderText = compileShader("text");
	glUseProgram(shaderText);

	shaderGaussian = compileShader("gaussian");
	glUseProgram(shaderGaussian);

	shaderSkybox = compileShader("skybox");
	glUseProgram(shaderSkybox);
	glUniform1i(glGetUniformLocation(shaderSkybox, "skyboxCube"), 0);

	shaderCubemap = compileShader("cubemap");
	glUseProgram(shaderCubemap);
	glUniform1i(glGetUniformLocation(shaderCubemap, "equirectangularMap"), 0);
	glUniform1i(glGetUniformLocation(shaderCubemap, "groundTex"), 1);

	shaderStandard = compileShader("standard");
	glUseProgram(shaderStandard);
	glUniform1i(glGetUniformLocation(shaderStandard, "diffuseMap"), 0);
	glUniform1i(glGetUniformLocation(shaderStandard, "normalMap"), 1);
	glUniform1i(glGetUniformLocation(shaderStandard, "specularMap"), 2);
	glUniform1i(glGetUniformLocation(shaderStandard, "emissionMap"), 3); 
	glUniform1i(glGetUniformLocation(shaderStandard, "reflectionSampler"), 4);
	glUniform1i(glGetUniformLocation(shaderStandard, "normalMap2"), 5);

	// Create our default textures.
	textureEmptyWhite = createTexture("emptyWhite", Albedo, glm::vec4(1, 1, 1, 1));
	textureEmptyBlack = createTexture("emptyBlack", Albedo, glm::vec4(0, 0, 0, 1));
	textureEmptyNormal = createTexture("emptyNormal", Normal, glm::vec4(0.5f, 0.5f, 1.0f, 0.5f));

	createFrameBuffer();

	viewportQuadVAO = createQuad(glm::vec2(-1, -1), glm::vec2(1, 1), glm::vec2(0, 0), glm::vec2(1, 1));
	loadingQuadVAO = createQuad(glm::vec2(-1, -1), glm::vec2(-0.3f, -0.4f), glm::vec2(0, 1), glm::vec2(1, 0));

	camera = new Camera();
	camera->setPosition(6.0f, 6.0f, 6.0f);
	camera->setViewTarget(0.0f, 3.0f, 0.0f);
	camera->update();

	postProcessing = new PostProcessing();

	skybox = new Skybox();
}

TextureAsset* Engine::getTextureEmptyWhite()
{
	return textureEmptyWhite;
}

TextureAsset* Engine::getTextureEmptyBlack()
{
	return textureEmptyBlack;
}

TextureAsset* Engine::getTextureEmptyNormal()
{
	return textureEmptyNormal;
}

PostProcessing* Engine::getPostProcessing()
{
	return postProcessing;
}

Skybox * Engine::getSkybox()
{
	return skybox;
}

bool Engine::getIsMouseCaptured()
{
	return mouseIsCaptured;
}

int Engine::buildGLWindow()
{
	// Error callback.
	glfwSetErrorCallback(error_callback);

	// Initialisation error.
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional.  
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version  
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2); //Request a specific OpenGL version  
	//glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing  
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  

	// Create window.
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Xenoville", NULL, NULL);

	// Window creation error.
	if (!window)
	{
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//This function makes the context of the specified window current on the calling thread.   
	glfwMakeContextCurrent(window);

	//Sets the key callback.  
	glfwSetKeyCallback(window, key_callback);

	glfwSetWindowSizeCallback(window, resize_callback);

	//Initialize GLEW.
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	//If GLEW hasn't initialized.
	if (err != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		return -1;
	}
	return 0;
}

Engine::Engine()
{
	
}

Engine::~Engine()
{
	if (window != nullptr)
	{
		//delete window;
	}
}

void Engine::refreshObjects()
{
	// Model Objects
	std::vector<ModelObject*>::iterator itModels = modelObjects.begin();
	while (itModels != modelObjects.end())
	{
		if ((*itModels)->destroyed)
		{
			ModelObject* model = *itModels;
			itModels = modelObjects.erase(itModels);
			delete(model);
		}
		else
		{
			itModels++;
		}
	}

	// Lights
	std::vector<Light*>::iterator itLights = lightObjects.begin();
	while (itLights != lightObjects.end())
	{
		if ((*itLights)->destroyed)
		{
			itLights = lightObjects.erase(itLights);
		}
		else
		{
			itLights++;
		}
	}
}

void Engine::renderWorld()
{
	std::vector<Material*> activeMaterials;
	std::vector<std::vector<std::pair<int, int>>> drawList;
	// List of materials. Each contains a list of meshes to draw, defined as (model index, part index).

	// Collect all materials.
	for (int i = 0; i < modelObjects.size(); i++) // Search each model.
	{
		ModelObject* model = modelObjects[i];
		for (int j = 0; j < model->materials.size(); j++) // Search each of its materials (also mesh part index).
		{
			Material* curMat = model->materials[j];

			int activeMaterialIndex = -1;
			int activeMaterialSize = activeMaterials.size();
			for (int k = 0; k < activeMaterialSize; k++) // Check if this material is already collected.
			{
				if (curMat == activeMaterials[k])
				{
					activeMaterialIndex = k; // Obtain its index if so.
					break;
				}
			}

			if (activeMaterialIndex >= 0) // If this material is already collected.
			{
				drawList[activeMaterialIndex].push_back(std::pair<int, int>(i, j)); // Add the mesh index and its mesh part index.
			}
			else
			{
				// Add the material to active material list.
				// Add the mesh index and its mesh part index.
				activeMaterials.push_back(curMat);
				drawList.push_back(std::vector<std::pair<int, int>>());
				drawList[drawList.size() - 1].push_back(std::pair<int, int>(i, j));
			}
		}
	}

	// Render opaque materials.
	for (int i = 0; i < activeMaterials.size(); i++)
	{
		Material* mat = activeMaterials[i];
		if (!mat->useBlending)
		{
			glUseProgram(mat->shaderProgram);

			mat->applyProperties();
			for (int j = 0; j < drawList[i].size(); j++)
			{
				std::pair<int, int> modelPartPair = drawList[i][j];
				int modelIndex = modelPartPair.first;
				int modelPartIndex = modelPartPair.second;
				ModelObject* m = modelObjects[modelIndex];
				m->getMesh()->draw(modelPartIndex, mat, m);
			}
			glActiveTexture(GL_TEXTURE0);
		}
	}

	// Render skybox.
	skybox->render();

	// Render transparent materials.
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	for (int i = 0; i < activeMaterials.size(); i++)
	{
		Material* mat = activeMaterials[i];
		if (mat->useBlending)
		{
			glUseProgram(mat->shaderProgram);
			glBlendFunc(mat->blendModeSrc, mat->blendModeDst);

			mat->applyProperties();
			for (int j = 0; j < drawList[i].size(); j++)
			{
				std::pair<int, int> modelPartPair = drawList[i][j];
				int modelIndex = modelPartPair.first;
				int modelPartIndex = modelPartPair.second;
				modelObjects[modelIndex]->getMesh()->draw(modelPartIndex, mat, modelObjects[modelIndex]);
			}
			glActiveTexture(GL_TEXTURE0);
		}
	}
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
}

void Engine::drawLoading()
{
	glClearColor(0.9f, 0.9f, 0.9f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(shaderText);
	glBindVertexArray(loadingQuadVAO);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	TextureAsset* loadingTex = loadTexture("Loading");
	glBindTexture(GL_TEXTURE_2D, loadingTex->glTextureId);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
	glDisable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glfwSwapBuffers(window);
}

int Engine::run()
{
	srand(clock());

	buildGLWindow();
	generateDefaultEngineObjects();
	drawLoading();

	gaus = new GaussianBlur((float)WINDOW_WIDTH * 1.0f, (float)WINDOW_HEIGHT * 1.0f);
	gaus->blurSize = 30;
	gaus->blurCount = 4;

	glClearColor(0.75f, 0.75f, 0.95f, 0.0f);
	glUseProgram(shaderStandard);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);
	glDepthFunc(GL_LEQUAL);

	// Hide and center the cursor so the mouse can freely move the camera.
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set mouse initial position and velocity.
	double mouseX = 0;
	double mouseY = 0;
	glfwGetCursorPos(window, &mouseX, &mouseY);
	mousePosition.x = mouseX;
	mousePosition.y = mouseY;
	previousMousePosition = mousePosition;

	game = new Game();
	game->start();
	
	//Main Loop  
	clock_t start = std::clock();
	clock_t old = start;
	do
	{
		// Update delta time.
		clock_t clk = clock();
		Engine::totalTime = (double)(clk - start) / double(CLOCKS_PER_SEC);
		Engine::frameDeltaTime = (double)(clk - old) / double(CLOCKS_PER_SEC);
		old = clk;

		// Update cursor.
		glfwGetCursorPos(window, &mouseX, &mouseY);
		mousePosition.x = floor(mouseX);
		mousePosition.y = floor(mouseY);
        mouseVelocity.x = floor(previousMousePosition.x - mouseX);
		mouseVelocity.y = floor(previousMousePosition.y - mouseY);
		previousMousePosition = mousePosition;

		// Update game.
		game->update();
		camera->update();

		refreshObjects();
		
		// Draw all objects to the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		renderWorld();

		// Generate a gaussian blur.
		GLuint blurTex = gaus->blurFrameBuffer(viewportQuadVAO, frameBufferColour);
		glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

		// Draw the contents of the framebuffer to the window,
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT);
		glUseProgram(shaderFramebuffer);
		glBindVertexArray(viewportQuadVAO);
		glDisable(GL_DEPTH_TEST);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, frameBufferColour);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, blurTex);
		glActiveTexture(GL_TEXTURE0);
		postProcessing->applyProperties(shaderFramebuffer);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glfwSwapBuffers(window);
		glfwPollEvents();

	}
	while (!glfwWindowShouldClose(window));

	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}
