#pragma once
#include<iostream> //cout
#include <fstream> //fstream
#include <vector> 
#include <ctime> 
#include <stdio.h>  
#include <stdlib.h> 

//Library for loading textures (Simple OpenGL Image Library)
//#include <SOIL.h>

#include <GL/glew.h>  

//Include GLFW  
#include <GLFW/glfw3.h>  

//Include matrix libraries
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"

//#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>

#include "Vertex.h"
#include "Game.h"
#include "Camera.h"
#include "ModelObject.h"
#include "MeshAsset.h"
#include "Light.h"
#include "PostProcessing.h"
#include "Skybox.h"
#include "GaussianBlur.h"

/// <summary>
/// A class used for tasks such as loading textures, meshes, compiling shaders etc. 
/// Also includes the primary run function.
/// </summary>
class Engine
{
	public:
		
		/// <summary>
		/// The window width.
		/// </summary>
		static int WINDOW_WIDTH;
		
		/// <summary>
		/// The window height.
		/// </summary>
		static int WINDOW_HEIGHT;
		
		/// <summary>
		/// Represents a texture type. Changes some of the settings used when loading textures.
		/// </summary>
		enum TextureType
		{
			/// <summary>
			/// Dunno what type of texture this is.
			/// </summary>
			Dunno,

			/// <summary>
			/// An albedo/base color texture.
			/// </summary>
			Albedo,

			/// <summary>
			/// Normal map textures.
			/// </summary>
			Normal,

			/// <summary>
			/// Specular map textures.
			/// </summary>
			Specular,

			/// <summary>
			/// A High Dynamic Range texture.
			/// </summary>
			HDR
		};

		Engine();
		~Engine();
	
		/// <summary>
		/// Runs the main engine. Starts the game and enters the game loop.
		/// </summary>
		/// <returns>Program exit state.</returns>
		int run();

		/// <summary>
		/// Sets up the OpenGL window using GLFW.
		/// </summary>
		/// <returns>-1 on failure.</returns>
		int buildGLWindow();
		
		/// <summary>
		/// Draws the loading VAO once.
		/// </summary>
		void drawLoading();
				
		/// <summary>
		/// Callback for when an OpenGL error occurs.
		/// </summary>
		/// <param name="error">The error code.</param>
		/// <param name="description">The error description.</param>
		static void error_callback(int error, const char* description);
		
		/// <summary>
		/// Callback for resizing a window..
		/// </summary>
		/// <param name="window">The window.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		static void resize_callback(GLFWwindow* window, int width, int height);
		
		/// <summary>
		/// Callback for when a key is pressed.
		/// </summary>
		/// <param name="window">The GL window.</param>
		/// <param name="key">The key that was pressed.</param>
		/// <param name="scancode">The scan code.</param>
		/// <param name="action">The action.</param>
		/// <param name="mods">The mods.</param>
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		
		/// <summary>
		/// Gets the shader compile status.
		/// </summary>
		/// <param name="shader">The shader.</param>
		/// <returns>True if the shader was compiled. Otherwise false.</returns>
		static bool getShaderCompileStatus(GLuint shader);
	
		/// <summary>
		/// Compiles vertex and fragment shader parts into a shader program.
		/// </summary>
		/// <param name="shaderAssetName">Name of the shader asset.</param>
		/// <returns>The GL identifier for the shader.</returns>
		static GLuint compileShader(std::string shaderAssetName);
		
		/// <summary>
		/// Creates the frame buffer.
		/// </summary>
		static void createFrameBuffer();
				
		/// <summary>
		/// Gets the path to a named asset.
		/// E.g. calling getAssetPath("cube.obj") will load the file at '/Assets/cube.obj'
		/// </summary>
		/// <param name="assetFileName">File name of the asset.</param>
		static std::string getAssetPath(std::string assetFileName, std::string type);
		
		/// <summary>
		/// Loads the mesh.
		/// </summary>
		/// <param name="assetName">Name of the asset.</param>
		/// <returns></returns>
		static MeshAsset* loadMesh(std::string assetName);
				
		/// <summary>
		/// Gets texture by asset name ("kitten" becomes "Assets/kitten.png") from the texture pool. If none is loaded, then attempts to load from disk then return.
		/// If cannot load then an error is printed and nullptr is returned.
		/// </summary>
		/// <param name="assetName">Name of the texture asset only without extension.</param>
		/// <returns>A pointer to a texture if one exists or can be loaded, otherwise nullptr.</returns>
		static TextureAsset* loadTexture(std::string assetName);

		/// <summary>
		/// Gets texture by asset name ("kitten" becomes "Assets/kitten.png") from the texture pool. If none is loaded, then attempts to load from disk then return.
		/// If cannot load then an error is printed and nullptr is returned.
		/// </summary>
		/// <param name="assetName">Name of the texture asset only without extension.</param>
		/// <param name="type">The type of the texture.</param>
		/// <returns>A pointer to a texture if one exists or can be loaded, otherwise nullptr.</returns>
		static TextureAsset* loadTexture(std::string assetName, TextureType type);
		
		/// <summary>
		/// Gets texture by asset name an extension ("kitten" and "bmp" becomes "Assets/kitten.bmp") from the texture pool. If none is loaded, then attempts to load from disk then return.
		/// If cannot load then an error is printed and nullptr is returned.
		/// </summary>
		/// <param name="assetName">Name of the texture asset only without extension.</param>
		/// <param name="type">The type of the texture.</param>
		/// <param name="extension">The extension to use.</param>
		/// <returns>A pointer to a texture if one exists or can be loaded, otherwise nullptr.</returns>
		static TextureAsset* loadTexture(std::string assetName, TextureType type, std::string extension);
				
		/// <summary>
		/// Creates a 1x1 pixel texture of a single colour with a given asset name. If a texture with the same name is already loaded, use that one.
		/// </summary>
		/// <param name="assetName">Name of the asset.</param>
		/// <param name="type">The type of the texture.</param>
		/// <param name="fromColour">The colour to generate.</param>
		/// <returns>A texture asset object.</returns>
		static TextureAsset* createTexture(std::string assetName, TextureType type, glm::vec4 fromColour);
	
		/// <summary>
		/// Creates a screen-space quad.
		/// </summary>
		/// <param name="min">The minimum bounds.</param>
		/// <param name="max">The maximum bounds.</param>
		/// <param name="texMin">The texture coord minimum.</param>
		/// <param name="texMax">The texture coord maximum.</param>
		/// <returns>A GLuin handle to the quad VAO.</returns>
		static GLuint createQuad(glm::vec2 min, glm::vec2 max, glm::vec2 texMin, glm::vec2 texMax);

		/// <summary>
		/// Gets the generic Phong-based shader.
		/// </summary>
		/// <returns>The GL number for the standard shader.</returns>
		static GLuint getShaderStandard();

		/// <summary>
		/// Gets the Physically Based Render (PBR) shader.
		/// </summary>
		/// <returns>The GL number for the PBR shader.</returns>
		static GLuint getShaderPBR();
		
		/// <summary>
		/// Gets the skybox shader.
		/// </summary>
		/// <returns>The GL number for the skybox shader.</returns>
		static GLuint getShaderSkybox();

		/// <summary>
		/// Gets the gaussian shader.
		/// </summary>
		/// <returns>The GL number for the gaussian shader.</returns>
		static GLuint getShaderGaussian();
		
		/// <summary>
		/// Gets the cubemap shader.
		/// </summary>
		/// <returns>The GL number for the cubemap shader.</returns>
		static GLuint getShaderCubemap();
		
		/// <summary>
		/// Gets the camera.
		/// </summary>
		/// <returns>The camera.</returns>
		static Camera* getCamera();
		
		/// <summary>
		/// Gets the delta time since the previous frame.
		/// </summary>
		/// <returns>The time in seconds since the previous frame.</returns>
		static double deltaTime();

		/// <summary>
		/// The total time since the game has started.
		/// </summary>
		/// <returns>The time in seconds since the game started.</returns>
		static double time();
		
		/// <summary>
		/// Registers the a ModelObject for rendering.
		/// </summary>
		/// <param name="object">The object.</param>
		static void registerModelObject(ModelObject* object);
		
		/// <summary>
		/// Registers a light for rendering.
		/// </summary>
		/// <param name="light">The light.</param>
		static void registerLight(Light* light);
		
		/// <summary>
		/// Gets the lights in the scene.
		/// </summary>
		/// <returns>A vector containing the lights in the scene.</returns>
		static std::vector<Light*> getLights();
		
		/// <summary>
		/// Retrieves lights of a given type maxed at a given value.
		/// </summary>
		/// <param name="max">The maximum number of lights.</param>
		/// <param name="type">The light type.</param>
		/// <returns>A vector containing the lights.</returns>
		static std::vector<Light*> getLightsOfType(int max, Light::LightType type);
				
		/// <summary>
		/// Gets the key state for the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>Returns GLFW_PRESS if the key is pressed, or GLFW_RELEASE if it is released.</returns>
		static int getKey(int key);
		
		/// <summary>
		/// Gets the mouse position.
		/// </summary>
		/// <returns>A vector containing the mouse position.</returns>
		static glm::vec2 getMousePosition();
		
		/// <summary>
		/// Gets the mouse velocity since the previous frame.
		/// </summary>
		/// <returns>A vector containing the difference of the mouse position this frame compared to the previous.</returns>
		static glm::vec2 getMouseVelocity();
		
		/// <summary>
		/// Generates the default engine objects such as shaders and empty textures.
		/// </summary>
		static void generateDefaultEngineObjects();
				
		/// <summary>
		/// Gets an empty white texture with RGBA value (1, 1, 1, 1).
		/// </summary>
		/// <returns></returns>
		static TextureAsset* getTextureEmptyWhite();
		
		/// <summary>
		/// Gets an empty black texture with RGBA value (0, 0, 0, 0).
		/// </summary>
		/// <returns></returns>
		static TextureAsset* getTextureEmptyBlack();
		
		/// <summary>
		///  Gets an empty normal bump texture with RGBA value (0.5, 0.5, 1, 0.5).
		/// </summary>
		/// <returns></returns>
		static TextureAsset* getTextureEmptyNormal();
		
		/// <summary>
		/// Gets the post processing effects.
		/// </summary>
		/// <returns>The post processing effects</returns>
		static PostProcessing* getPostProcessing();
		
		/// <summary>
		/// Gets the skybox.
		/// </summary>
		/// <returns>The skybox.</returns>
		static Skybox* getSkybox();
		
		/// <summary>
		/// Gets whether the is mouse captured.
		/// </summary>
		/// <returns>True if the mouse is captured, otherwise false.</returns>
		static bool getIsMouseCaptured();

	private:
		static Game* game;
		static Camera* camera;
		static double totalTime;
		static double frameDeltaTime;

		static glm::vec2 mousePosition;
		static glm::vec2 previousMousePosition;
		static glm::vec2 mouseVelocity;
		static bool mouseIsCaptured;

		static GLFWwindow* window;

		static GLuint frameBuffer;
		static GLuint frameBufferColour;
		static GLuint depthRenderBuffer;
		static GLuint viewportQuadVAO;
		static GLuint loadingQuadVAO;
		
		static GLuint shaderStandard; // The standard shader.
		static GLuint shaderPBR; // The physically based shader.
		static GLuint shaderSkybox; // The unlit skybox shader.
		static GLuint shaderFramebuffer; // The frame buffer shader. (For post-processing etc.)
		static GLuint shaderGaussian; // The shader used for performing gaussian blur.
		static GLuint shaderText; // The text shader.
		static GLuint shaderCubemap; // The cubemap shader.

		static TextureAsset* textureEmptyWhite; // An empty white texture. RGBA(1,1,1,1)
		static TextureAsset* textureEmptyBlack; // An empty black texture. RGBA(0,0,0,0)
		static TextureAsset* textureEmptyNormal; // An empty normal texture. RGBA(0.5,0.5,1,0.5)

		static std::vector<TextureAsset*> loadedTextures; // Texture cache.
		static std::vector<MeshAsset*> loadedMeshes; // Mesh cache.

		static std::vector<ModelObject*> modelObjects; // Currently active models being rendered and updated in the engine.
		static std::vector<Light*> lightObjects; // Currently active lights to use in rendering.

		static PostProcessing* postProcessing; // The post-processing effects.
		static Skybox* skybox; // The skybox that is rendered around the scene.
		static GaussianBlur* gaus;
		
		/// <summary>
		/// Refreshes all ModelObjects in the engine and performs rudimentary tasks such as drawing and purging destroyed objects.
		/// </summary>
		void refreshObjects();
		
		/// <summary>
		/// Renders the world.
		/// </summary>
		void renderWorld();
		
		
};