#include <GLM/gtx/color_space.hpp>
#include "Game.h"
#include "ModelObject.h"
#include "Light.h"
#include "Engine.h"
#include "MaterialStandard.h"
#include "Cubemap.h"
#include "Plant.h"

// References to our objects and materials.
ModelObject* objectLandscape;
ModelObject* objectKittenBox;
ModelObject* objectGem;
ModelObject* objectBall;
ModelObject* objectMonkey;
ModelObject* objectBigTree;
ModelObject* objectArrow;
ModelObject* objectEmissive;
std::vector<ModelObject*> trees;

ModelObject* objectShed;
ModelObject* objectFence;
std::vector<Plant*> plants;
std::vector<glm::vec3> plantLocations;
std::vector<ModelObject*> dirtObjects;

// Refrences to any materials we will need later.
MaterialStandard* gemMaterial;
MaterialStandard* ballMaterial;
MaterialStandard* emissionMaterial;
MaterialStandard* dirtMoundMaterial;
MaterialStandard* materialPuddle;
MaterialStandard* materialWater;

// References to any lights.
Light* sun;


void Game::start()
{
	TextureAsset* skyTexture = Engine::loadTexture("Field", Engine::HDR, "hdr");
	TextureAsset* reflectionGroundTexture = Engine::loadTexture("GrassHDDiffuse", Engine::Albedo);

	glm::vec2 reflectionGroundTexScale = glm::vec2(10, 10);
	glm::vec3 reflectionGroundColour = glm::vec3(0.5f, 0.5f, 0.5f);

	Engine::getSkybox()->skyCube = Cubemap::render(skyTexture, 4096, 4096);
	Engine::getSkybox()->skyReflectionCube = Cubemap::render(skyTexture, 512, 512, reflectionGroundTexture, reflectionGroundTexScale, reflectionGroundColour);

	Plant::generateMaterials(); // Generate all the plant materials.

	// The directional sun.
	sun = new Light();
	sun->setPosition(0, 25, 0);
	sun->intensity = 0.75f;
	sun->type = Light::Directional;
	sun->setRotationEuler(230, 110, 0);
	sun->colour = glm::vec3(0.85f, 0.92f, 1.0f);
	sun->ambientPower = glm::vec3(0.1f, 0.1f, 0.14f);
	sun->specularPower = glm::vec3(0.85f, 0.88f, 1.0f) * 3.0f;	

	// Landscape
	MaterialStandard* materialLandscape = new MaterialStandard();
	materialLandscape->diffuseMap = Engine::loadTexture("GrassHDDiffuse", Engine::Albedo);
	materialLandscape->normalMap = Engine::loadTexture("GrassHDNormals", Engine::Normal);
	materialLandscape->specularMap = Engine::loadTexture("GrassHDSpecular", Engine::Specular);
	materialLandscape->specular = glm::vec3(1);
	materialLandscape->reflectionColour = glm::vec3(1);
	materialLandscape->texScale = glm::vec2(10, 10);
	materialLandscape->shininess = 10;

	materialWater = new MaterialStandard();
	materialWater->tintColour = glm::vec3(0, 0.04, 0.08);
	materialWater->hasNormal2 = true;
	materialWater->normalMap = Engine::loadTexture("CloudNormal", Engine::Normal);
	materialWater->normalMapMultiplier = 0.15f;
	materialWater->texScale = glm::vec2(5.0f, 5.0f);
	materialWater->normalMap2 = Engine::loadTexture("CloudNormal", Engine::Normal);
	materialWater->normalMapMultiplier2 = 0.15f;
	materialWater->texScale2 = glm::vec2(10.0f, 10.0f);
	materialWater->reflectionColour = glm::vec3(1);
	materialWater->setTransparent();
	materialWater->alpha = 0.6f;
	//materialWater->texScale2 = glm::vec2(0.2f, 0.2f);
	materialWater->shininess = 10000;
	materialWater->fresnelExponent = 0.8f;

	objectLandscape = new ModelObject();
	objectLandscape->setPosition(0, 0, 0);
	objectLandscape->setRotationEuler(0.0f, 0.0f, 0.0f);
	objectLandscape->setScale(50.0f, 50.0f, 50.0f);
	objectLandscape->loadMesh("Landscape");
	objectLandscape->setMaterial(0, materialWater);
	objectLandscape->setMaterial(1, materialLandscape);

	// Arrow pointing in the direction of the sunlight.
	MaterialStandard* arrowMaterial = new MaterialStandard();
	arrowMaterial->tintColour = glm::vec3(0.6f, 0.6f, 0.6f);
	arrowMaterial->reflectionColour = glm::vec3(1);
	objectArrow = new ModelObject();
	objectArrow->setPosition(sun->getPosition());
	objectArrow->setRotation(sun->getRotation());
	objectArrow->loadMesh("Arrow", arrowMaterial);

	MaterialStandard* mirror = new MaterialStandard();
	mirror->fresnelExponent = 0;
	mirror->reflectionColour = glm::vec3(0.5f);
	mirror->tintColour = glm::vec3(0);
	objectBall = new ModelObject();
	objectBall->setScale(2, 2, 2);
	objectBall->setPosition(0, 2, 0);
	objectBall->loadMesh("Ball", mirror);

	// Gem mesh.
	gemMaterial = new MaterialStandard();
	gemMaterial->diffuseMap = Engine::loadTexture("Plasma", Engine::Albedo);
	gemMaterial->normalMap = Engine::loadTexture("dirtNormals", Engine::Normal);
	gemMaterial->tintColour = glm::vec3(0.8f, 0, 0);
	gemMaterial->shininess = 50;
	gemMaterial->specular = glm::vec3(4);
	gemMaterial->normalMapMultiplier = 0.2f;
	gemMaterial->reflectionColour = glm::vec3(1);
	gemMaterial->fresnelExponent = 1.0f;
	objectGem = new ModelObject();
	objectGem->setPosition(-2, 2, -3);
	objectGem->setRotationEuler(0.0f, 0.0f, 0.0f);
	objectGem->setScale(1.0f, 1.0f, 1.0f);
	objectGem->loadMesh("RockCrystal", gemMaterial);


	// A vertex coloured stand, and multi-material monkey.
	MaterialStandard* monkeyMaterial = new MaterialStandard();
	monkeyMaterial->diffuseMap = Engine::loadTexture("MetalDiffuse", Engine::Albedo);
	monkeyMaterial->normalMap = Engine::loadTexture("MetalNormals", Engine::Normal);
	monkeyMaterial->specularMap = Engine::loadTexture("MetalSpecular", Engine::Specular);
	monkeyMaterial->specular = glm::vec3(4, 4, 4);
	monkeyMaterial->shininess = 60;
	monkeyMaterial->normalMapMultiplier = 0.1f;
	monkeyMaterial->texScale = glm::vec2(1, 1);
	monkeyMaterial->reflectionColour = glm::vec3(1);
	MaterialStandard* standMaterial = new MaterialStandard();
	standMaterial->diffuseMap = Engine::loadTexture("Kitten", Engine::Albedo);
	standMaterial->tintColour = glm::vec3(1.0f, 1.0f, 1.0f);
	standMaterial->texScale = glm::vec2(2, 2);
	objectMonkey = new ModelObject();
	objectMonkey->setPosition(10.0f, 0.0f, 14);
	objectMonkey->setRotationEuler(0.0f, -90.0f, 0.0f);
	objectMonkey->loadMesh("MonkeyOnStand");
	objectMonkey->setMaterial(0, standMaterial);
	objectMonkey->setMaterial(1, monkeyMaterial);


	// Slime
	MaterialStandard* slimeEyes = new MaterialStandard();
	slimeEyes->diffuseMap = Engine::loadTexture("VacantStare", Engine::Albedo);
	slimeEyes->specular = glm::vec3(2, 2, 2);
	slimeEyes->shininess = 300;
	slimeEyes->reflectionColour = glm::vec3(2);
	slimeEyes->fresnelExponent = 2;
	MaterialStandard* slimeTransparent1 = new MaterialStandard();
	slimeTransparent1->normalMap = Engine::loadTexture("CloudNormal", Engine::Normal);
	slimeTransparent1->normalMapMultiplier = 0.2f;
	slimeTransparent1->texScale = glm::vec2(20, 20);
	slimeTransparent1->tintColour = glm::vec3(0, 1.0f, 0.3f) * 0.3f;
	slimeTransparent1->specular = glm::vec3(6, 6, 6);
	slimeTransparent1->shininess = 300;
	slimeTransparent1->alpha = 0.2f;
	slimeTransparent1->reflectionColour = glm::vec3(1);
	slimeTransparent1->fresnelExponent = 0.7;
	slimeTransparent1->setTransparent();
	MaterialStandard* slimeTransparent2 = new MaterialStandard();
	slimeTransparent2->normalMap = Engine::loadTexture("CloudNormal", Engine::Normal);
	slimeTransparent2->normalMapMultiplier = 0.3f;
	slimeTransparent2->texScale = glm::vec2(5, 5);
	slimeTransparent2->tintColour = glm::vec3(0, 1.0f, 0.3f) * 0.3f;
	slimeTransparent2->specular = glm::vec3(6, 6, 6);
	slimeTransparent2->shininess = 300;
	slimeTransparent2->alpha = 0.2f;
	slimeTransparent2->reflectionColour = glm::vec3(1);
	slimeTransparent2->fresnelExponent = 0.7;
	slimeTransparent2->setTransparent();
	MaterialStandard* slimeCore = new MaterialStandard();
	slimeCore->tintColour = glm::vec3(0, 1.0f, 0.3f) * 0.3f;
	slimeCore->specular = glm::vec3(6, 6, 6);
	slimeCore->shininess = 300;
	MaterialStandard* slimeRibbon = new MaterialStandard();
	slimeRibbon->diffuseMap = Engine::loadTexture("Plasma", Engine::Albedo);
	slimeRibbon->normalMap = Engine::loadTexture("CloudNormal", Engine::Albedo);
	slimeRibbon->specularMap = Engine::loadTexture("ShedConcreteSpecular", Engine::Specular);
	slimeRibbon->tintColour = glm::vec3(0.7, 0.1, 0.1);
	slimeRibbon->specular = glm::vec3(1, 1, 1);
	slimeRibbon->shininess = 20;
	slimeRibbon->texScale = glm::vec2(20, 20);
	MaterialStandard* slimeRing = new MaterialStandard();
	slimeRing->tintColour = glm::vec3(0, 0, 0);
	slimeRing->specular = glm::vec3(6, 6, 6);
	slimeRing->shininess = 1000;
	slimeRing->reflectionColour = glm::vec3(1, 1, 1);
	slimeRing->fresnelExponent = 0;

	ModelObject* objectSlime = new ModelObject();
	objectSlime->setPosition(-18, 0, 0);
	objectSlime->setScale(2.0f);
	objectSlime->loadMesh("Slime");
	objectSlime->setMaterial(0, slimeCore); // Blob Inner
	objectSlime->setMaterial(1, slimeTransparent2); // Blob Middle
	objectSlime->setMaterial(2, slimeTransparent1); // Blob Outer
	objectSlime->setMaterial(3, slimeEyes); // Eye Left
	objectSlime->setMaterial(4, slimeEyes); // Eye Right
	objectSlime->setMaterial(5, slimeRibbon); // Ribbon Flap 2
	objectSlime->setMaterial(6, slimeRing); // Ring
	objectSlime->setMaterial(7, slimeRibbon); // Ribbon 2
	objectSlime->setMaterial(8, slimeRibbon); // Ribbon Flap
	objectSlime->setMaterial(9, slimeRibbon); // Ribbon
	

	// Fencing
	MaterialStandard* fenceMaterialCable = new MaterialStandard();
	fenceMaterialCable->diffuseMap = Engine::loadTexture("ShedAlimin", Engine::Albedo);
	fenceMaterialCable->normalMap = Engine::loadTexture("ShedAliminNormal", Engine::Normal);
	fenceMaterialCable->specularMap = Engine::loadTexture("ShedAliminSpecular", Engine::Specular);
	fenceMaterialCable->texScale = glm::vec2(1, 60);

	MaterialStandard* fenceMaterialPost = new MaterialStandard();
	fenceMaterialPost->diffuseMap = Engine::loadTexture("BambooWood", Engine::Albedo);
	fenceMaterialPost->normalMap = Engine::loadTexture("BambooWoodNormal", Engine::Normal);
	fenceMaterialPost->specularMap = Engine::loadTexture("BambooWoodSpecular", Engine::Specular);
	fenceMaterialPost->texScale = glm::vec2(2, 2);

	MaterialStandard* fenceMaterialGate = new MaterialStandard();
	fenceMaterialGate->diffuseMap = Engine::loadTexture("ShedAlimin", Engine::Albedo);
	fenceMaterialGate->normalMap = Engine::loadTexture("ShedAliminNormal", Engine::Normal);
	fenceMaterialGate->specularMap = Engine::loadTexture("ShedAliminSpecular", Engine::Specular);
	fenceMaterialGate->texScale = glm::vec2(20, 40);
	fenceMaterialGate->shininess = 2000;
	fenceMaterialGate->fresnelExponent = 0.75f;
	fenceMaterialGate->reflectionColour = glm::vec3(0.3f);

	MaterialStandard* fenceMaterialMetal = new MaterialStandard();
	fenceMaterialMetal->diffuseMap = Engine::loadTexture("ShedAlimin", Engine::Albedo);
	fenceMaterialMetal->normalMap = Engine::loadTexture("ShedAliminNormal", Engine::Normal);
	fenceMaterialMetal->specularMap = Engine::loadTexture("ShedAliminSpecular", Engine::Specular);
	fenceMaterialMetal->texScale = glm::vec2(2, 4);
	fenceMaterialMetal->shininess = 2000;
	fenceMaterialMetal->fresnelExponent = 0.75f;

	objectFence = new ModelObject();
	objectFence->setPosition(-20.0f, 0.0f, 25.0f);
	objectFence->setRotationEuler(0, 0, 0);
	objectFence->setScale(3, 3, 3);
	objectFence->loadMesh("Fencing");
	objectFence->setMaterial(0, fenceMaterialMetal); // Hinge
	objectFence->setMaterial(1, fenceMaterialCable); // Fence Wire
	objectFence->setMaterial(2, fenceMaterialGate); // Fence
	objectFence->setMaterial(3, fenceMaterialCable); // Cable
	objectFence->setMaterial(4, fenceMaterialPost); // Poles

	//Decorative Garden Shed
	//https://www.turbosquid.com/FullPreview/Index.cfm/ID/968592

	MaterialStandard* shedMaterialAlumin = new MaterialStandard();
	shedMaterialAlumin->diffuseMap = Engine::loadTexture("ShedAlimin", Engine::Albedo);
	shedMaterialAlumin->normalMap = Engine::loadTexture("ShedAliminNormal", Engine::Normal);
	shedMaterialAlumin->specularMap = Engine::loadTexture("ShedAliminSpecular", Engine::Specular);
	shedMaterialAlumin->normalMapMultiplier = 0.5f;
	shedMaterialAlumin->shininess = 200;
	shedMaterialAlumin->specular = glm::vec3(2);

	MaterialStandard* shedMaterialWood = new MaterialStandard();
	shedMaterialWood->diffuseMap = Engine::loadTexture("ShedWood", Engine::Albedo);
	shedMaterialWood->normalMap = Engine::loadTexture("ShedWoodNormal", Engine::Normal);
	shedMaterialWood->specularMap = Engine::loadTexture("ShedWoodSpecular", Engine::Specular);
	shedMaterialWood->normalMapMultiplier = 1.0f;
	shedMaterialWood->shininess = 40;
	shedMaterialWood->specular = glm::vec3(1.5f);

	MaterialStandard* shedMaterialWood2 = new MaterialStandard();
	shedMaterialWood2->diffuseMap = Engine::loadTexture("ShedWood2", Engine::Albedo);
	shedMaterialWood2->normalMap = Engine::loadTexture("ShedWood2Normal", Engine::Normal);
	shedMaterialWood2->specularMap = Engine::loadTexture("ShedWood2Specular", Engine::Specular);
	shedMaterialWood2->shininess = 40;
	shedMaterialWood2->specular = glm::vec3(1.5f);
	shedMaterialWood2->normalMapMultiplier = 1.0f;

	MaterialStandard* shedMaterialWood3 = new MaterialStandard();
	shedMaterialWood3->diffuseMap = Engine::loadTexture("ShedWood3", Engine::Albedo);
	shedMaterialWood3->normalMap = Engine::loadTexture("ShedWood3Normal", Engine::Normal);
	shedMaterialWood3->specularMap = Engine::loadTexture("ShedWood3Specular", Engine::Specular);
	shedMaterialWood3->normalMapMultiplier = 1.0f;
	shedMaterialWood3->shininess = 40;
	shedMaterialWood3->specular = glm::vec3(1.5f);

	MaterialStandard* shedMaterialTin = new MaterialStandard();
	shedMaterialTin->diffuseMap = Engine::loadTexture("ShedTin", Engine::Albedo);
	shedMaterialTin->normalMap = Engine::loadTexture("ShedTinNormal", Engine::Normal);
	shedMaterialTin->specularMap = Engine::loadTexture("ShedTinSpecular", Engine::Specular);
	shedMaterialTin->shininess = 200;
	shedMaterialTin->specular = glm::vec3(2);
	shedMaterialTin->texScale = glm::vec2(0.7f, 4);
	shedMaterialTin->reflectionColour = glm::vec3(1, 0.3f, 0.3f);
	shedMaterialTin->fresnelExponent = 0.5f;
	shedMaterialTin->normalMapMultiplier = 0.3f;

	MaterialStandard* shedMaterialConcrete = new MaterialStandard();
	shedMaterialConcrete->diffuseMap = Engine::loadTexture("ShedConcrete", Engine::Albedo);
	shedMaterialConcrete->normalMap = Engine::loadTexture("ShedConcreteNormal", Engine::Normal);
	shedMaterialConcrete->specularMap = Engine::loadTexture("ShedConcreteSpecular", Engine::Specular);
	shedMaterialConcrete->normalMapMultiplier = 0.4f;

	MaterialStandard* materialGlass = new MaterialStandard();
	materialGlass->shininess = 400;
	materialGlass->reflectionColour = glm::vec3(1);
	materialGlass->fresnelExponent = 0.3f;
	materialGlass->setTransparent();
	materialGlass->alpha = 0.1f;

	objectShed = new ModelObject();
	objectShed->setPosition(-20, -0.2f, 45);
	objectShed->setScale(1.4f, 1.4f, 1.4f);
	objectShed->setRotationEuler(0, 90, 0);
	objectShed->loadMesh("Shed");
	objectShed->setMaterial(0, shedMaterialTin); //Roof
	objectShed->setMaterial(1, shedMaterialWood2); //Walls
	objectShed->setMaterial(2, shedMaterialWood); //Framing
	objectShed->setMaterial(3, shedMaterialConcrete); //Floor
	objectShed->setMaterial(4, shedMaterialWood); //Doorknob
	objectShed->setMaterial(5, shedMaterialWood3); //Door
	objectShed->setMaterial(6, shedMaterialAlumin); //Ventilation Shaft
	objectShed->setMaterial(7, materialGlass); //Window

	dirtMoundMaterial = new MaterialStandard();
	dirtMoundMaterial->diffuseMap = Engine::loadTexture("dirt", Engine::Albedo);
	dirtMoundMaterial->normalMap = Engine::loadTexture("dirtNormals", Engine::Normal);
	dirtMoundMaterial->specularMap = Engine::loadTexture("dirtSpecular", Engine::Specular);
	dirtMoundMaterial->reflectionColour = glm::vec3(0.4f);
	dirtMoundMaterial->specular = glm::vec3(0.3f);
	dirtMoundMaterial->shininess = 30;

	// Treeeeeeeeeees
	glm::vec3 treeBoundsMin = glm::vec3(-390, 0, -390);
	glm::vec3 treeBoundsMax = glm::vec3(390, 0, 390);
	glm::vec3 exclusionBoundsMainMin = glm::vec3(-60, 0, -5);
	glm::vec3 exclusionBoundsMainMax = glm::vec3(15, 0, 60);
	glm::vec3 highPolyBoundsMin = glm::vec3(exclusionBoundsMainMin.x - 90, 0, exclusionBoundsMainMin.y - 100);
	glm::vec3 highPolyBoundsMax = glm::vec3(exclusionBoundsMainMax.x + 70, 0, exclusionBoundsMainMax.y + 150);

	glm::vec3 exclusionBoundsWaterMin = glm::vec3(70, 0, -280);
	glm::vec3 exclusionBoundsWaterMax = glm::vec3(320, 0, 330);

	std::pair<glm::vec3, glm::vec3> mainAreaBounds = std::pair<glm::vec3, glm::vec3>(exclusionBoundsMainMin, exclusionBoundsMainMax);
	std::pair<glm::vec3, glm::vec3> waterBounds = std::pair<glm::vec3, glm::vec3>(exclusionBoundsWaterMin, exclusionBoundsWaterMax);
	std::vector<std::pair<glm::vec3, glm::vec3>> exclusionBounds;
	exclusionBounds.push_back(mainAreaBounds);
	exclusionBounds.push_back(waterBounds);

	// Bounds testing
	/*
	glm::vec3 boundsMin = exclusionBoundsWaterMin;
	glm::vec3 boundsMax = exclusionBoundsWaterMax;
	MaterialStandard* minMat = new MaterialStandard();
	minMat->tintColour = glm::vec3(1, 0, 0);
	MaterialStandard* maxMat = new MaterialStandard();
	maxMat->tintColour = glm::vec3(0, 0, 1);

	ModelObject* o1 = new ModelObject();
	o1->loadMesh("Ball", minMat);
	o1->setPosition(boundsMin.x, 0, boundsMin.z);
	o1->setScale(1);
	ModelObject* o2 = new ModelObject();
	o2->loadMesh("Ball", new MaterialStandard());
	o2->setPosition(boundsMin.x, 0, boundsMax.z);
	o2->setScale(0.8);
	ModelObject* o3 = new ModelObject();
	o3->loadMesh("Ball", maxMat);
	o3->setPosition(boundsMax.x, 0, boundsMax.z);
	o3->setScale(0.9);
	ModelObject* o4 = new ModelObject();
	o4->loadMesh("Ball", new MaterialStandard());
	o4->setPosition(boundsMax.x, 0, boundsMin.z);
	o4->setScale(0.7);
	*/

	generateRandomTrees(30, 100, treeBoundsMin, treeBoundsMax, exclusionBounds, highPolyBoundsMin, highPolyBoundsMax, 0.6f, 2.0f);

	generatePuddles(60, highPolyBoundsMin, highPolyBoundsMax, 0.8f, 2.0f);

	// Plants
	generatePlantLocations();
	generatePlants();
}

void Game::update()
{
	handleInput();
	
	objectArrow->setPosition(sun->getPosition());
	objectArrow->setRotation(sun->getRotation());

	float delta = Engine::deltaTime();

	// Cycle the hue spectrum for the gem material.
	glm::vec3 hsv = glm::hsvColor(gemMaterial->tintColour);
	hsv.x += delta * 120.0f;

	Plant::materials[Plant::Crystal]->tintColour = glm::rgbColor(hsv);
	gemMaterial->tintColour = glm::rgbColor(hsv);

	materialWater->texOffset += glm::vec2(0.2f, 0.9) * delta * 0.02f;
	materialWater->texOffset2 += glm::vec2(-0.2f, -0.9) * delta * 0.02f;
}

double Game::lerp(double a, double b, double t)
{
	return a * (1 - t) + b * t;
}

double Game::nextDouble()
{
	return ((double)rand() / (RAND_MAX));
}

double Game::nextRange(double min, double max)
{
	double r = nextDouble();
	return lerp(min, max, r);
}

bool Game::inBounds(glm::vec3 point, glm::vec3 min, glm::vec3 max)
{
	return (point.x >= min.x && point.x <= max.x) && (point.y >= min.y && point.y <= max.y) && (point.z >= min.z && point.z <= max.z);
}

void Game::generateRandomTrees(int countHighPoly, int countLowPoly, glm::vec3 minBounds, glm::vec3 maxBounds, std::vector<std::pair<glm::vec3, glm::vec3>> exclusionZones, glm::vec3 highDefMin, glm::vec3 highDefMax, float minScale, float maxScale)
{
	MaterialStandard* bigTreeBark = new MaterialStandard();
	bigTreeBark->diffuseMap = Engine::loadTexture("Bark", Engine::Albedo);
	bigTreeBark->normalMap = Engine::loadTexture("BarkNormals", Engine::Normal);
	bigTreeBark->specularMap = Engine::loadTexture("BarkSpecular", Engine::Specular);
	bigTreeBark->tintColour = glm::vec3(1.0f, 1.0f, 1.0f);
	bigTreeBark->texScale = glm::vec2(4, 1);
	MaterialStandard* bigTreeLeaves = new MaterialStandard();
	bigTreeLeaves->diffuseMap = Engine::loadTexture("Leaf", Engine::Albedo);
	bigTreeLeaves->normalMap = Engine::loadTexture("LeafNormals", Engine::Normal);
	bigTreeLeaves->specularMap = Engine::loadTexture("LeafSpecular", Engine::Specular);
	bigTreeLeaves->texScale = glm::vec2(2.0f, 1.0f);
	bigTreeLeaves->texOffset = glm::vec2(0.5f, 0.0f);
	bigTreeLeaves->cutoutThreshold = 0.5f;
	bigTreeLeaves->shininess = 40;
	bigTreeLeaves->specular = glm::vec3(5);

	// Generate a bunch of high poly trees.
	for (int i = 0; i < countHighPoly; i++)
	{
		glm::vec3 pos = glm::vec3(0);
		bool validPosition = false;
		while (!validPosition)
		{
			pos = glm::vec3(nextRange(highDefMin.x, highDefMax.x), nextRange(highDefMin.y, highDefMax.y), nextRange(highDefMin.z, highDefMax.z));
			bool inExclusionArea = false;
			for (int j = 0; j < exclusionZones.size(); j++)
			{
				if (inBounds(pos, exclusionZones[j].first, exclusionZones[j].second))
				{
					inExclusionArea = true;
					break;
				}
			}
			
			if (!inExclusionArea)
			{
				validPosition = true;
			}
		}

		std::string treeMesh = "NiceTree2";
		
		ModelObject* tree = new ModelObject();
		tree->setPosition(pos);
		tree->setRotationEuler(0, nextDouble() * 360, 0);
		tree->setScale(nextRange(minScale, maxScale));
		tree->loadMesh(treeMesh);
		tree->setMaterial(0, bigTreeBark);
		tree->setMaterial(1, bigTreeLeaves);
	}

	// Generate a bunch of low poly trees.
	for (int i = 0; i < countLowPoly; i++)
	{
		glm::vec3 pos = glm::vec3(0);
		bool validPosition = false;
		while (!validPosition)
		{
			pos = glm::vec3(nextRange(minBounds.x, maxBounds.x), nextRange(minBounds.y, maxBounds.y), nextRange(minBounds.z, maxBounds.z));

			bool inExclusionArea = inBounds(pos, highDefMin, highDefMax);
			if (inExclusionArea)
			{
				continue;
			}

			for (int j = 0; j < exclusionZones.size(); j++)
			{
				if (inBounds(pos, exclusionZones[j].first, exclusionZones[j].second))
				{
					inExclusionArea = true;
					break;
				}
			}

			if (!inExclusionArea)
			{
				validPosition = true;
			}
		}

		std::string treeMesh = "NiceTreeLow";

		ModelObject* tree = new ModelObject();
		tree->setPosition(pos);
		tree->setRotationEuler(0, nextDouble() * 360, 0);
		tree->setScale(nextRange(minScale, maxScale));
		tree->loadMesh(treeMesh);
		tree->setMaterial(0, bigTreeBark);
		tree->setMaterial(1, bigTreeLeaves);
	}
}

void Game::generatePuddles(int count, glm::vec3 minBounds, glm::vec3 maxBounds, float minScale, float maxScale)
{
	materialPuddle = new MaterialStandard();
	materialPuddle->tintColour = glm::vec3(0);
	materialPuddle->normalMap = Engine::loadTexture("CloudNormal", Engine::Normal);
	materialPuddle->normalMapMultiplier = 0.02f;
	materialPuddle->texScale = glm::vec2(0.2f, 0.2f);
	materialPuddle->reflectionColour = glm::vec3(1);
	materialPuddle->setTransparent();
	materialPuddle->alpha = 0.05f;
	materialPuddle->shininess = 10000;
	materialPuddle->fresnelExponent = 1.7f;

	for (int i = 0; i < count; i++)
	{
		glm::vec3 pos = glm::vec3(0);
		bool validPosition = false;
		while (!validPosition)
		{
			pos = glm::vec3(nextRange(minBounds.x, maxBounds.x), nextRange(minBounds.y, maxBounds.y), nextRange(minBounds.z, maxBounds.z));
			validPosition = true;
		}

		ModelObject* puddle = new ModelObject();
		puddle->setPosition(pos);
		puddle->setRotationEuler(0, nextDouble() * 360, 0);
		puddle->setScale(nextRange(minScale, maxScale));
		puddle->loadMesh("Puddle", materialPuddle);
	}
}

void Game::generatePlants()
{
	for (Plant* p : plants)
	{
		p->destroy();
		delete(p);
	}

	plants = std::vector<Plant*>();

	int numStemAssets = 10;
	int numLeaveAssets = 9;

	for (int i = 0; i < plantLocations.size(); i++)
	{
		glm::vec3 location = plantLocations[i];
		Plant::Appearance randAppearanceStem = (Plant::Appearance)(rand() % (Plant::Appearance::ENUM_APPEARANCE_COUNT - 1));
		Plant::Appearance randAppearanceLeaf = (Plant::Appearance)(rand() % (Plant::Appearance::ENUM_APPEARANCE_COUNT - 1));
		Plant::Size randSize = (Plant::Size)(rand() % Plant::Size::ENUM_SIZE_COUNT);
		int randLeaf = 1 + rand() % numLeaveAssets;
		int randStem = 1 + rand() % numStemAssets;

		Plant* p = new Plant();
		p->BuildPlant(randStem, randLeaf, randSize, randAppearanceStem, randAppearanceLeaf, location);
		plants.push_back(p);
	}
}

void Game::generatePlantLocations()
{
	for (ModelObject* o : dirtObjects)
	{
		o->destroy();
	}

	dirtObjects = std::vector<ModelObject*>();
	plantLocations = std::vector<glm::vec3>();

	float plantSeparation = 10;
	float plantsX = -30;
	float plantsY = 0.25f;
	float plantsZ = 10;
	int plantCountX = 3;
	int plantCountZ = 3;

	glm::vec3 center = glm::vec3(plantsX + (0.5f * (plantCountX - 1) * plantSeparation), plantsY, plantsZ + (0.5f * (plantCountZ - 1) * plantSeparation));
	Engine::getCamera()->setViewTarget(center);


	for (int x = 0; x < plantCountX; x++)
	{
		for (int z = 0; z < plantCountZ; z++)
		{
			glm::vec3 dirPos = glm::vec3(plantsX + (x * plantSeparation), 0, plantsZ + (z * plantSeparation));
			glm::vec3 plantPos = glm::vec3(dirPos.x, plantsY, dirPos.z);

			ModelObject* dirt = new ModelObject();
			dirt->setPosition(dirPos);
			dirt->setScale(glm::vec3(nextRange(1.9f, 2.4f), 2.0f, nextRange(1.9f, 2.4f)));
			dirt->setRotationEuler(0, nextRange(0, 359), 0);
			dirt->loadMesh("DirtMound", dirtMoundMaterial);

			dirtObjects.push_back(dirt);
			plantLocations.push_back(plantPos);
		}
	}
}

void Game::handleInput()
{
	float delta = Engine::deltaTime();
	Camera* cam = Engine::getCamera();

	// Move around.
	glm::vec3 moveDir = glm::vec3();
	float moveSpeed = 8;
	if (Engine::getKey(GLFW_KEY_LEFT_SHIFT))
	{
		moveSpeed *= 4;
	}

	if (Engine::getKey(GLFW_KEY_W))
	{
		moveDir += cam->forward();
	}

	if (Engine::getKey(GLFW_KEY_S))
	{
		moveDir -= cam->forward();
	}

	if (Engine::getKey(GLFW_KEY_A))
	{
		moveDir += cam->left();
	}

	if (Engine::getKey(GLFW_KEY_D))
	{
		moveDir -= cam->left();
	}

	if (Engine::getKey(GLFW_KEY_Q))
	{
		moveDir -= cam->up();
	}

	if (Engine::getKey(GLFW_KEY_E))
	{
		moveDir += cam->up();
	}
	cam->translate(moveDir * delta * moveSpeed);

	//Generate Plants
	if (Engine::getKey(GLFW_KEY_G))
	{
		generatePlants();
	}

	// Modify some values intensity.
	float lightSpeed = 4.0f;
	if (Engine::getKey(GLFW_KEY_X))
	{
		gemMaterial->normalMapMultiplier += delta * 1;
	}
	if (Engine::getKey(GLFW_KEY_Z))
	{
		gemMaterial->normalMapMultiplier -= delta * 1;
	}

	if (Engine::getKey(GLFW_KEY_U))
	{
		sun->intensity -= delta * lightSpeed;
	}
	if (Engine::getKey(GLFW_KEY_I))
	{
		sun->intensity += delta * lightSpeed;
	}
	if (Engine::getKey(GLFW_KEY_O))
	{
		sun->rotate(glm::vec3(0, delta * 180, 0));
	}
	if (Engine::getKey(GLFW_KEY_P))
	{
		sun->rotate(glm::vec3(0, delta * -180, 0));
	}

	if (Engine::getKey(GLFW_KEY_K))
	{
		sun->rotateLocal(glm::vec3(delta * 180, 0, 0));
	}
	if (Engine::getKey(GLFW_KEY_L))
	{
		sun->rotateLocal(glm::vec3(delta * -180, 0, 0));
	}

	if (Engine::getKey(GLFW_KEY_H))
	{
		Engine::getPostProcessing()->bloomIntensity -= delta * 20;
	}
	if (Engine::getKey(GLFW_KEY_J))
	{
		Engine::getPostProcessing()->bloomIntensity += delta * 20;
	}

	if (Engine::getKey(GLFW_KEY_N))
	{
		Engine::getPostProcessing()->bloomContrast -= delta * 10;
	}
	if (Engine::getKey(GLFW_KEY_M))
	{
		Engine::getPostProcessing()->bloomContrast += delta * 10;
	}

	if (Engine::getKey(GLFW_KEY_V))
	{
		Engine::getPostProcessing()->bloomRamp -= delta * 10;
	}
	if (Engine::getKey(GLFW_KEY_B))
	{
		Engine::getPostProcessing()->bloomRamp += delta * 10;
	}
	
	if (Engine::getKey(GLFW_KEY_1))
	{
		Engine::getSkybox()->exposure -= delta * 1;
	}
	if (Engine::getKey(GLFW_KEY_2))
	{
		Engine::getSkybox()->exposure += delta * 1;
	}

	// Look around.
	if (Engine::getIsMouseCaptured())
	{
		float lookSpeed = 3.0f * delta;
		cam->rotate(glm::vec3(0, Engine::getMouseVelocity().x * lookSpeed, 0));
		cam->rotateLocal(glm::vec3(-Engine::getMouseVelocity().y * lookSpeed, 0, 0));
	}
}

void Game::onKeyDown(int key)
{
	if (key == GLFW_KEY_F)
	{
		Engine::getCamera()->lockViewTarget = !Engine::getCamera()->lockViewTarget;
	}
	else if (key == GLFW_KEY_TAB)
	{
		std::cout << "Bloom Intensity: " << Engine::getPostProcessing()->bloomIntensity << std::endl;
		std::cout << "Bloom Contrast: " << Engine::getPostProcessing()->bloomContrast << std::endl;
		std::cout << "Bloom Ramp: " << Engine::getPostProcessing()->bloomRamp << std::endl;
	}
}
