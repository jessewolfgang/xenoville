#pragma once
#include <vector>
#include <glm.hpp>

/// <summary>
/// Represents the main game loop.
/// </summary>
class Game
{
public:

	/// <summary>
	/// Raised at the start of the game.
	/// </summary>
	void start();
	
	/// <summary>
	/// Raised each game loop update.
	/// </summary>
	void update();

	/// <summary>
	/// Linear interpolation from a to b.
	/// </summary>
	/// <param name="a">Start point.</param>
	/// <param name="b">End point.</param>
	/// <param name="t">Interpolant in the range 0 - 1.</param>
	/// <returns>A value between a and b given t.</returns>
	static double lerp(double a, double b, double t);
	
	/// <summary>
	/// Gets a random double in the range 0 - 1.
	/// </summary>
	/// <returns>A double between 0 and 1.</returns>
	static double nextDouble();
	
	/// <summary>
	/// Gets a random double in the given range.
	/// </summary>
	/// <param name="min">The minimum.</param>
	/// <param name="max">The maximum.</param>
	/// <returns>A double between min and max.</returns>
	static double nextRange(double min, double max);
			
	/// <summary>
	/// Whether the point is withing the bounds.
	/// </summary>
	/// <param name="point">The point.</param>
	/// <param name="min">The minimum cuboid corner.</param>
	/// <param name="max">The maximum cuboid corner.</param>
	/// <returns>True if within the bounds. Otherwise false.</returns>
	static bool inBounds(glm::vec3 point, glm::vec3 min, glm::vec3 max);

	/// <summary>
	/// Generates a bunch of random trees.
	/// </summary>
	/// <param name="count">The number of trees.</param>
	/// <param name="minBounds">The minimum bounds.</param>
	/// <param name="maxBounds">The maximum bounds.</param>
	/// <param name="minScale">The minimum scale.</param>
	/// <param name="maxScale">The maximum scale.</param>
	void generateRandomTrees(int countHighPoly, int countLowPoly, glm::vec3 minBounds, glm::vec3 maxBounds, std::vector<std::pair<glm::vec3, glm::vec3>> exclusionZones, glm::vec3 highDefMin, glm::vec3 highDefMax, float minScale, float maxScale);
	
	void generatePuddles(int count, glm::vec3 minBounds, glm::vec3 maxBounds, float minScale, float maxScale);

	/// <summary>
	/// Generates a set of plants.
	/// </summary>
	void generatePlants();

	void generatePlantLocations();
	
	/// <summary>
	/// Handles user input like camera movement.
	/// </summary>
	void handleInput();

	/// <summary>
	/// Raised when a key is pressed.
	/// </summary>
	/// <param name="key">The key that was pressed.</param>
	void onKeyDown(int key);
};

