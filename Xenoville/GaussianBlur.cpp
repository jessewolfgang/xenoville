#include "GaussianBlur.h"
#include <GLM/glm.hpp>
#include "Engine.h"

GaussianBlur::GaussianBlur(float width, float height)
{
	this->width = width;
	this->height = height;

	blurSize = 8;
	blurCount = 10;

	glGenFramebuffers(2, pingPongFBO);
	glGenTextures(2, pingPongTextures);
	for (unsigned int i = 0; i < 2; i++)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pingPongFBO[i]);
		glBindTexture(GL_TEXTURE_2D, pingPongTextures[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, (int)width, (int)height, 0, GL_RGB, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingPongTextures[i], 0);
	}
}

GaussianBlur::~GaussianBlur()
{
}

GLuint GaussianBlur::blurFrameBuffer(GLuint viewportQuadVAO, GLuint tex)
{
	bool horizontal = true;
	bool first_iteration = true;
	GLuint shaderProgram = Engine::getShaderGaussian();
	glUseProgram(shaderProgram);
	glUniform1f(glGetUniformLocation(shaderProgram, "blurSize"), blurSize);
	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(viewportQuadVAO);
	for (unsigned int i = 0; i < blurCount; i++)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pingPongFBO[horizontal]);
		glViewport(0, 0, width, height);
		glUniform1i(glGetUniformLocation(shaderProgram, "horizontal"), horizontal);
		//glBindTexture(GL_TEXTURE_2D, tex);
		glBindTexture(GL_TEXTURE_2D, first_iteration ? tex : pingPongTextures[!horizontal]);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		horizontal = !horizontal;
		if (first_iteration)
			first_iteration = false;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindVertexArray(0);
	glEnable(GL_DEPTH_TEST);

	return pingPongTextures[horizontal];
}
