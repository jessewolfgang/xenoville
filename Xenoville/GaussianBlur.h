#pragma once
#include <GL/glew.h>

/// <summary>
/// An implementation of Gaussian Blur.
/// Derived from information at https://learnopengl.com/Advanced-Lighting/Bloom.
/// </summary>
class GaussianBlur
{
public:
	
	/// <summary>
	/// The amount of spread applied to the gaussian blur.
	/// </summary>
	float blurSize;
	
	/// <summary>
	/// The number of times to apply the blur.
	/// </summary>
	int blurCount;

	GaussianBlur(float width, float height);
	~GaussianBlur();
	
	/// <summary>
	/// Blurs the frame buffer.
	/// </summary>
	/// <param name="viewportVAO">The viewport vertex array.</param>
	/// <param name="tex">The texture to blur.</param>
	/// <returns>A handle to the output texture.</returns>
	GLuint blurFrameBuffer(GLuint viewportVAO, GLuint tex);

private:
	GLuint pingPongFBO[2];
	GLuint pingPongTextures[2];

	int width;
	int height;
};

