#pragma once

/// <summary>
/// An endpoint node on a plant structure.
/// </summary>
class Leaf 
{
public:
	Leaf();
	~Leaf();

};

