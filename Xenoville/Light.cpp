#include "Light.h"
#include "Engine.h"

Light::Light()
{
	type = Point;
	colour = glm::vec3(1, 1, 1);
	intensity = 1;
	ambientPower = glm::vec3(0.1f, 0.1f, 0.1f);
	specularPower = glm::vec3(1, 1, 1);

	Engine::registerLight(this);
}

Light::~Light()
{
}
