#pragma once
#include "Object.h"

/// <summary>
/// An object that emits light.
/// </summary>
/// <seealso cref="Object" />
class Light : public Object
{
public:
		
	/// <summary>
	/// An enumeration describing what type of light this is.
	/// </summary>
	enum LightType
	{		
		/// <summary>
		/// A light emitting from a single point radially.
		/// </summary>
		Point,
		
		/// <summary>
		/// A light emitting from a uniform direction.
		/// </summary>
		Directional
	};

	/// <summary>
	/// The colour of the light.
	/// </summary>
	glm::vec3 colour;
	
	/// <summary>
	/// The intensity of the light.
	/// </summary>
	float intensity;
	
	/// <summary>
	/// The type of light.
	/// </summary>
	LightType type;
	
	/// <summary>
	/// The ambient power.
	/// </summary>
	glm::vec3 ambientPower;
	
	/// <summary>
	/// The specular power.
	/// </summary>
	glm::vec3 specularPower;


	Light();
	~Light();

};

