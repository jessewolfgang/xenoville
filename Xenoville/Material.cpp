#include "Material.h"
#include <vector>>
#include "Light.h"
#include "Engine.h"

Material::Material(GLuint shaderProgram)
{
	this->shaderProgram = shaderProgram;
	this->useBlending = false;
	this->blendModeSrc = GL_SRC_COLOR;
	this->blendModeDst = GL_DST_COLOR;
}

Material::~Material()
{
}

void Material::setInt(const std::string & name, int value)
{
	glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), value);
}

void Material::setFloat(const std::string & name, float value)
{
	glUniform1f(glGetUniformLocation(shaderProgram, name.c_str()), value);
}

void Material::setBool(const std::string & name, bool value)
{
	glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), (int)value);
}

void Material::setVec2(const std::string & name, glm::vec2 value)
{
	glUniform2fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void Material::setVec3(const std::string & name, glm::vec3 value)
{
	glUniform3fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void Material::setVec4(const std::string & name, glm::vec4 value)
{
	glUniform4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void Material::setTexture(GLenum textureSlot, TextureAsset* texture, TextureAsset* fallback)
{
	glActiveTexture(textureSlot);
	if (texture != nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, texture->glTextureId);
	}
	else if (fallback != nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, fallback->glTextureId);
	}
	glActiveTexture(GL_TEXTURE0);
}

void Material::setCube(GLenum textureSlot, GLuint cube)
{
	glActiveTexture(textureSlot);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cube);
	glActiveTexture(GL_TEXTURE0);
}

void Material::applyLights()
{
	std::vector<Light*> pointLights = Engine::getLightsOfType(8, Light::Point);
	std::vector<Light*> dirLights = Engine::getLightsOfType(3, Light::Directional);

	for (int i = 0; i < pointLights.size(); i++)
	{
		Light* l = pointLights[i];
		std::string iStr = std::to_string(i);

		setVec3("pointLights[" + iStr + "].position", l->getPosition());
		setVec4("pointLights[" + iStr + "].colour", glm::vec4(l->colour, 1));
		setFloat("pointLights[" + iStr + "].intensity", l->intensity);
		setVec3("pointLights[" + iStr + "].ambientPower", l->ambientPower);
		setVec3("pointLights[" + iStr + "].specularPower", l->specularPower);
	}

	for (int i = 0; i < dirLights.size(); i++)
	{
		Light* l = dirLights[i];
		std::string iStr = std::to_string(i);

		setVec3("directionalLights[" + iStr + "].direction", l->forward());
		setVec4("directionalLights[" + iStr + "].colour", glm::vec4(l->colour, 1));
		setFloat("directionalLights[" + iStr + "].intensity", l->intensity);
		setVec3("directionalLights[" + iStr + "].ambientPower", l->ambientPower);
		setVec3("directionalLights[" + iStr + "].specularPower", l->specularPower);
	}
}

void Material::setTransparent()
{
	useBlending = true;
	blendModeSrc = GL_SRC_ALPHA;
	blendModeDst = GL_ONE_MINUS_SRC_ALPHA;
}

void Material::applyProperties()
{
}
