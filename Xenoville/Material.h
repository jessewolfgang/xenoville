#pragma once
#include <map>
#include <GL/glew.h>
#include <glm.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "TextureAsset.h"

/// <summary>
/// An abstract base class for materials.
/// </summary>
class Material
{
public:

	/// <summary>
	/// The source blending factor.
	/// </summary>
	GLenum blendModeSrc;

	/// <summary>
	/// The destination blending factor.
	/// </summary>
	GLenum blendModeDst;

	/// <summary>
	/// Whether to use colour blending.
	/// </summary>
	bool useBlending;

	Material(GLuint shaderProgram);
	~Material();

	/// <summary>
	/// The shader program identifier.
	/// </summary>
	GLuint shaderProgram;

	/// <summary>
	/// Sets the blending factors to Src=GL_SRC_ALPHA, Dst=GL_ONE_MINUS_SRC_ALPHA.
	/// </summary>
	void setTransparent();

	/// <summary>
	/// Used by subclasses to apply OpenGL properties before rendering.
	/// </summary>
	virtual void applyProperties() = 0;

protected:

	/// <summary>
	/// Sets an int.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="value">The value.</param>
	void setInt(const std::string &name, int value);

	/// <summary>
	/// Sets a float.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="value">The value.</param>
	void setFloat(const std::string &name, float value);

	/// <summary>
	/// Sets a bool.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="value">if set to <c>true</c> [value].</param>
	void setBool(const std::string &name, bool value);

	/// <summary>
	/// Sets a vec2.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="value">The value.</param>
	void setVec2(const std::string &name, glm::vec2 value);

	/// <summary>
	/// Sets a vec3.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="value">The value.</param>
	void setVec3(const std::string &name, glm::vec3 value);

	/// <summary>
	/// Sets a vec4.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="value">The value.</param>
	void setVec4(const std::string &name, glm::vec4 value);

	/// <summary>
	/// Sets the texture for a sampler.
	/// </summary>
	/// <param name="textureSlot">The texture slot.</param>
	/// <param name="texture">The texture to set.</param>
	/// <param name="fallback">The fallback texture if the texture passed is nullptr.</param>
	void setTexture(GLenum textureSlot, TextureAsset* texture, TextureAsset* fallback);

	/// <summary>
	/// Sets a cubemap.
	/// </summary>
	/// <param name="textureSlot">The texture slot.</param>
	/// <param name="cube">The cube.</param>
	void setCube(GLenum textureSlot, GLuint cube);

	/// <summary>
	/// Applies the lights to the shader using a standard layout.
	/// </summary>
	void applyLights();

};

