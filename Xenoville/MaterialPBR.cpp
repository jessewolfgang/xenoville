#include "MaterialPBR.h"
#include "Engine.h"

MaterialPBR::MaterialPBR() : Material(Engine::getShaderPBR())
{
}

MaterialPBR::~MaterialPBR()
{
}

void MaterialPBR::applyProperties()
{
	applyLights();
}
