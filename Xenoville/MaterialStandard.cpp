#include "MaterialStandard.h"
#include "Engine.h"

MaterialStandard::MaterialStandard() : Material(Engine::getShaderStandard())
{
	texScale = glm::vec2(1.0f, 1.0f);
	texOffset = glm::vec2(0.0f, 0.0f);
	texScale2 = glm::vec2(1.0f, 1.0f);
	texOffset2 = glm::vec2(0.0f, 0.0f);
	tintColour = glm::vec3(1.0f, 1.0f, 1.0f);
	hasNormal2 = false;
	alpha = 1.0f;
	shininess = 10;
	normalMapMultiplier = 1.0f;
	normalMapMultiplier2 = 1.0f;
	fresnelExponent = 1.3f;
	cutoutThreshold = -1.0f;
	specular = glm::vec3(1);
	emission = glm::vec3(0);
	reflectionColour = glm::vec3(0.2);
}

MaterialStandard::~MaterialStandard()
{
}

void MaterialStandard::applyProperties()
{
	applyLights();

	setVec2("texScale", texScale);
	setVec2("texOffset", texOffset);
	setVec2("texScale2", texScale2);
	setVec2("texOffset2", texOffset2);
	setVec3("tintColour", tintColour);
	setFloat("alpha", alpha);
	setFloat("shininess", shininess);
	setFloat("normalMapMultiplier", normalMapMultiplier);
	setFloat("normalMapMultiplier2", normalMapMultiplier2);
	setFloat("fresnelExponent", fresnelExponent);
	setFloat("cutoutThreshold", cutoutThreshold);
	setVec3("specularMuliplier", specular);
	setVec3("emission", emission);
	setVec3("reflectionIntensity", reflectionColour);
	setBool("hasNormal2", hasNormal2);

	setTexture(GL_TEXTURE0, diffuseMap, Engine::getTextureEmptyWhite());
	setTexture(GL_TEXTURE1, normalMap, Engine::getTextureEmptyNormal());
	setTexture(GL_TEXTURE2, specularMap, Engine::getTextureEmptyWhite());
	setTexture(GL_TEXTURE3, emissionMap, Engine::getTextureEmptyWhite());
	setCube(GL_TEXTURE4, Engine::getSkybox()->skyReflectionCube);
	setTexture(GL_TEXTURE5, normalMap2, Engine::getTextureEmptyNormal());
}
