#pragma once
#include "Material.h"
#include "TextureAsset.h"

/// <summary>
/// A material that uses the standard Blinn-Phong shader.
/// </summary>
/// <seealso cref="Material" />
class MaterialStandard : public Material
{
public:

	/// <summary>
	/// The values to scale the texture coordinates by.
	/// Larger numbers, make more frequently tiling textures.
	/// </summary>
	glm::vec2 texScale;
	
	/// <summary>
	/// The texture offset.
	/// </summary>
	glm::vec2 texOffset;

	/// <summary>
	/// The values to scale the texture coordinates of the second normal map.
	/// </summary>
	glm::vec2 texScale2;

	/// <summary>
	/// The texture offset of the second normal map.
	/// </summary>
	glm::vec2 texOffset2;
	
	/// <summary>
	/// Whether this material has a second normal texture. Uses texScale2 and texOffset2.
	/// </summary>
	bool hasNormal2;

	/// <summary>
	/// The alpha transparency.
	/// </summary>
	float alpha;

	/// <summary>
	/// The shininess of the material.
	/// </summary>
	float shininess;

	/// <summary>
	/// The intensity multiplier of the normal map.
	/// </summary>
	float normalMapMultiplier;

	/// <summary>
	/// The intensity multiplier of the second normal map.
	/// </summary>
	float normalMapMultiplier2;

	/// <summary>
	/// The fresnel exponent.
	/// A value of 0 results in a uniformly reflective metal, while higher values reduce the overall reflectivity towards shallow viewing angles.
	/// </summary>
	float fresnelExponent;
	
	/// <summary>
	/// The alpha value at which the fragment is discarded.
	/// A negative value results in no cutout.
	/// </summary>
	float cutoutThreshold;

	/// <summary>
	/// The tint colour for the material.
	/// </summary>
	glm::vec3 tintColour;

	/// <summary>
	/// The intensity multiplier of the material's specular highlights.
	/// </summary>
	glm::vec3 specular;

	/// <summary>
	/// The emission colour.
	/// </summary>
	glm::vec3 emission;

	/// <summary>
	/// The reflection colour.
	/// </summary>
	glm::vec3 reflectionColour;

	/// <summary>
	/// The diffuse texture map.
	/// </summary>
	TextureAsset* diffuseMap;

	/// <summary>
	/// The normal map.
	/// </summary>
	TextureAsset* normalMap;

	/// <summary>
	/// The second normal map.
	/// </summary>
	TextureAsset* normalMap2;

	/// <summary>
	/// The specular map.
	/// </summary>
	TextureAsset* specularMap;

	/// <summary>
	/// The emission map.
	/// </summary>
	TextureAsset* emissionMap;

	MaterialStandard();
	~MaterialStandard();

	void applyProperties() override;
};

