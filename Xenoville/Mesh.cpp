#include "GLM/gtc/type_ptr.hpp"
#include "Mesh.h"
#include "Vertex.h"
#include "Engine.h"


Mesh::Mesh()
{

}

Mesh::Mesh(std::string assetName)
{
	this->assetName = assetName;
	this->asset = nullptr;
	buildVertexBuffer();
}

Mesh::Mesh(MeshAsset* asset)
{
	this->assetName = asset->assetName;
	this->asset = asset;
	buildVertexBuffer();
}

Mesh::~Mesh()
{
	for (int i = 0; i < VAOs.size(); i++)
	{
		glBindVertexArray(VAOs[i]);
		glDeleteBuffers(1, &VBOs[i]);
		glDeleteVertexArrays(1, &VAOs[i]);
	}
}

void Mesh::draw(int partIndex, Material* material, Object* hostParent)
{
	if (hostParent->destroyed) return;
	MeshPart* part = asset->parts[partIndex];
		
	if (material == nullptr)
	{
		std::cout << "Mesh " << part->meshPartName << " is missing material." << std::endl;
		VAOs.erase(VAOs.begin() + partIndex);
	}

	// Set model, view and projection matrices.
	GLint uniModel = glGetUniformLocation(material->shaderProgram, "model");
	glUniformMatrix4fv(uniModel, 1, GL_FALSE, glm::value_ptr(hostParent->getTransformMatrix()));

	GLint uniView = glGetUniformLocation(material->shaderProgram, "view");
	glm::mat4 viewMat = Engine::getCamera()->getViewMatrix();
	glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(Engine::getCamera()->getViewMatrix()));

	GLint uniProj = glGetUniformLocation(material->shaderProgram, "proj");
	glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(Engine::getCamera()->getProjectionMatrix()));

	GLint uniViewPos = glGetUniformLocation(material->shaderProgram, "viewPos");
	glUniform3fv(uniViewPos, 1, glm::value_ptr(Engine::getCamera()->getPosition()));

	// Bind and draw vertex array.
	glBindVertexArray(VAOs[partIndex]);
	glDrawArrays(GL_TRIANGLES, 0, part->vertexCount);
	glBindVertexArray(0);
}

void Mesh::buildVertexBuffer()
{
	VAOs = std::vector<GLuint>();

	if (asset == nullptr)
	{
		asset = Engine::loadMesh(assetName);
	}
	
	for (int i = 0; i < asset->parts.size(); i++)
	{
		MeshPart* part = asset->parts[i];

		GLuint vao;
		GLuint vbo;

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);

		VAOs.push_back(vao);
		VBOs.push_back(vbo);

		if (part->vertexCount == 0)
		{
			std::cout << "Model Empty!!" << std::endl;
			return;
		}

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * part->vertices.size(), &part->vertices[0], GL_STATIC_DRAW);

		GLint shaderIndex = 0;
		//GLint shaderIndex = glGetAttribLocation(Engine::getShaderStandard(), "position");
		glEnableVertexAttribArray(shaderIndex);
		glVertexAttribPointer(shaderIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Vertex::position));
		
		// = glGetAttribLocation(Engine::getShaderStandard(), "normal");
		shaderIndex = 1;
		glEnableVertexAttribArray(shaderIndex);
		glVertexAttribPointer(shaderIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Vertex::normal));

		//shaderIndex = glGetAttribLocation(Engine::getShaderStandard(), "colour");
		shaderIndex = 2;
		glEnableVertexAttribArray(shaderIndex);
		glVertexAttribPointer(shaderIndex, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Vertex::colour));

		//shaderIndex = glGetAttribLocation(Engine::getShaderStandard(), "texcoord");
		shaderIndex = 3;
		glEnableVertexAttribArray(shaderIndex);
		glVertexAttribPointer(shaderIndex, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Vertex::texCoords));

		//shaderIndex = glGetAttribLocation(Engine::getShaderStandard(), "tangent");
		shaderIndex = 4;
		glEnableVertexAttribArray(shaderIndex);
		glVertexAttribPointer(shaderIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Vertex::tangent));


		glBindVertexArray(0);
	}
		
}
