#pragma once
#include <string>
#include <vector> 
#include <GL/glew.h>

#include "Object.h"
#include "Material.h"
#include "MeshAsset.h"


/// <summary>
/// A mesh object that can be drawn to the screen.
/// </summary>
class Mesh
{
public:
	
	MeshAsset* asset;

	Mesh();

	/// <summary>
	/// Initializes a new instance of the <see cref="Mesh"/> class.
	/// </summary>
	/// <param name="assetName">Name of the 3D mesh asset to load.</param>
	Mesh(std::string assetName);
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Mesh"/> class.
	/// </summary>
	/// <param name="asset">The asset to use.</param>
	Mesh(MeshAsset* asset);

	~Mesh();
	
	/// <summary>
	/// Draws a part of the mesh.
	/// </summary>
	/// <param name="partIndex">The index of the mesh part to draw.</param>
	/// <param name="material">The material to draw with.</param>
	/// <param name="hostParent">The object rendering this mesh.</param>
	void draw(int partIndex, Material* material, Object* hostParent);


private:
	std::string assetName;
	std::vector<GLuint> VAOs;
	std::vector<GLuint> VBOs;
	//GLuint VAO;
	//GLuint VBO;
	//int vertexCount;
	
	
	/// <summary>
	/// Builds the vertex array.
	/// </summary>
	void buildVertexBuffer();
};

