#pragma once
#include <string>
#include <vector>
#include <GL/glew.h>
#include "Vertex.h"
#include "MeshPart.h"

/// <summary>
/// A 3D mesh asset.
/// </summary>
struct MeshAsset
{
	/// <summary>
	/// The asset name of the mesh.
	/// </summary>
	std::string assetName;
		
	/// <summary>
	/// The parts in this mesh.
	/// </summary>
	std::vector<MeshPart*> parts;
};