#pragma once

#include<string>
#include<vector>

/// <summary>
/// Represents part of a mesh that can have a particular material.
/// </summary>
struct MeshPart
{	
	/// <summary>
	/// The name of this mesh part.
	/// </summary>
	std::string meshPartName;

	/// <summary>
	/// The vertices of the mesh.
	/// </summary>
	std::vector<Vertex> vertices;

	/// <summary>
	/// The number of vertices.
	/// </summary>
	int vertexCount;
};

