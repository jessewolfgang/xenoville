#include "ModelObject.h"
#include "Engine.h"

ModelObject::ModelObject()
{
	useSingleMaterial = true;
	materials = std::vector<Material*>();
	Engine::registerModelObject(this);
}

ModelObject::~ModelObject()
{
	delete mesh;
}

void ModelObject::loadMesh(std::string assetName)
{
	mesh = new Mesh(assetName);
}

void ModelObject::loadMesh(std::string assetName, Material* mat)
{
	mesh = new Mesh(assetName);
	materials.push_back(mat);
}

Mesh* ModelObject::getMesh()
{
	return mesh;
}

void ModelObject::render()
{
	if (materials.size() == 0 || mesh == nullptr)
	{
		std::cout << "Error drawing mesh! Deleting";
		destroy();
		return;
	}
	
	int count = mesh->asset->parts.size();
	for (int i = 0; i < count; i++)
	{
		Material* mat = nullptr;
		if (i < materials.size())
			mat = materials[i];

		if (mat != nullptr && !mat->useBlending)
		{
			mesh->draw(i, mat, this);
		}
	}
}

void ModelObject::renderTransparent()
{
	if (materials.size() == 0 || mesh == nullptr)
	{
		std::cout << "Error drawing transparent mesh! Deleting";
		destroy();
		return;
	}

	for (int i = 0; i < mesh->asset->parts.size(); i++)
	{
		Material* mat = nullptr;
		if (i < materials.size())
			mat = materials[i];

		if (mat != nullptr && mat->useBlending)
		{
			mesh->draw(i, mat, this);
		}
	}
}

void ModelObject::setMaterial(Material* material)
{
	if (materials.size() >= 1)
	{
		materials[0] = material;
	}
	else
	{
		materials.push_back(material);
	}
}

void ModelObject::setMaterial(int materialIndex, Material * material)
{
	useSingleMaterial = false;
	if (materials.size() <= materialIndex)
	{
		materials.resize(materialIndex + 1, nullptr);
	}
	materials[materialIndex] = material;
}
