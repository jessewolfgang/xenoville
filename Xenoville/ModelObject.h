#pragma once
#include "Object.h"
#include "Mesh.h"

/// <summary>
/// Represents an object with a 3D model.
/// </summary>
/// <seealso cref="Object" />
class ModelObject : public Object
{
public:	
	
	/// <summary>
	/// Whether to use a single material or not.
	/// </summary>
	bool useSingleMaterial;
	
	/// <summary>
	/// The materials for this object.
	/// </summary>
	std::vector<Material*> materials;

	ModelObject();
	~ModelObject();
	
	/// <summary>
	/// Loads the mesh into the ModelObject.
	/// </summary>
	/// <param name="assetName">Name of the mesh asset.</param>
	void loadMesh(std::string assetName);

	/// <summary>
	/// Loads the mesh into the ModelObject with a given material.
	/// </summary>
	/// <param name="assetName">Name of the mesh asset.</param>
	/// <param name="mat">The material to use.</param>
	void loadMesh(std::string assetName, Material* mat);
		
	/// <summary>
	/// Gets the mesh for this object.
	/// </summary>
	/// <returns>The mesh object.</returns>
	Mesh* getMesh();
		
	/// <summary>
	/// Renders the opaque parts of the model.
	/// If the material is null, then nothing is drawn.
	/// </summary>
	void render();
	
	/// <summary>
	/// Renders the transparent parts of the model.
	/// If the material is null, then nothing is drawn.
	/// </summary>
	void renderTransparent();
	
	/// <summary>
	/// Sets the material for material index 0.
	/// </summary>
	/// <param name="material">The material.</param>
	void setMaterial(Material* material);

	/// <summary>
	/// Sets the material for a specified index.
	/// Calling this method sets useSingleMaterial to true.
	/// </summary>
	/// <param name="materialIndex">Index of the material.</param>
	/// <param name="material">The material.</param>
	void setMaterial(int materialIndex, Material* material);

protected:
	Mesh* mesh;
	

};

