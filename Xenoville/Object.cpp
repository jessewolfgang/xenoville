#include <GLM/gtc/quaternion.hpp>
#include "Object.h"

Object::Object()
{
	destroyed = false;
	requiresTransformRebuild = false;
	setPosition(0.0f, 0.0f, 0.0f);
	setRotationEuler(glm::vec3(0.0f, 0.0f, 0.0f));
	setScale(glm::vec3(1.0f, 1.0f, 1.0f));
}

Object::~Object()
{
}

void Object::setChild(Object* child)
{
	child->parent = this;
	children.push_back(child);
	child->markRebuildTransform();
}

glm::vec3 Object::getPosition()
{
	return glm::vec3(positionMatrix[3].x, positionMatrix[3].y, positionMatrix[3].z);
}

void Object::setPosition(glm::vec3 pos)
{
	if (getPosition() == pos)
		return;

	this->positionMatrix = glm::translate(glm::mat4(), pos);
	markRebuildTransform();
}

void Object::setPosition(float x, float y, float z)
{
	setPosition(glm::vec3(x, y, z));
}

glm::vec3 Object::getRotationEuler()
{
	return glm::eulerAngles(rotationQuat);
}

glm::quat Object::getRotation()
{
	return rotationQuat;
}

void Object::rotate(glm::vec3 rotation)
{
	if (rotation == glm::vec3(0))
		return;

	glm::quat rot = glm::rotate(glm::quat(), rotation.y, glm::vec3(0, 1, 0));
	rot = glm::rotate(rot, rotation.x, glm::vec3(1, 0, 0));
	rot = glm::rotate(rot, rotation.z, glm::vec3(0, 0, 1));

	rotationQuat = rot * rotationQuat; // Multiply the rotation on the LHS.
	markRebuildTransform();
}

void Object::rotateLocal(glm::vec3 rotation)
{
	if (rotation == glm::vec3(0))
		return;

	glm::quat rot = glm::rotate(glm::quat(), rotation.y, glm::vec3(0, 1, 0));
	rot = glm::rotate(rot, rotation.x, glm::vec3(1, 0, 0));
	rot = glm::rotate(rot, rotation.z, glm::vec3(0, 0, 1));

	rotationQuat = rotationQuat * rot; // Multiply the rotation on the RHS.
	markRebuildTransform();
}

void Object::translate(glm::vec3 translation)
{
	if (translation == glm::vec3(0))
		return;

	positionMatrix = glm::translate(positionMatrix, translation);
	markRebuildTransform();
}

void Object::setRotationEuler(glm::vec3 rotation)
{
	if (getRotationEuler() == rotation)
		return;

	rotationQuat = glm::rotate(glm::quat(), rotation.y, glm::vec3(0, 1, 0));
	rotationQuat = glm::rotate(rotationQuat, rotation.x, glm::vec3(1, 0, 0));
	rotationQuat = glm::rotate(rotationQuat, rotation.z, glm::vec3(0, 0, 1));
	markRebuildTransform();
}

void Object::setRotationEuler(double x, double y, double z)
{
	setRotationEuler(glm::vec3(x, y, z));
}

void Object::setRotation(glm::quat quat)
{
	if (getRotation() == quat)
		return;

	rotationQuat = quat;
	markRebuildTransform();
}

glm::vec3 Object::getScale()
{
	return glm::vec3(scaleMatrix[0].x, scaleMatrix[1].y, scaleMatrix[2].z);
}

void Object::setScale(glm::vec3 scale)
{
	if (getScale() == scale)
		return;

	scaleMatrix = glm::scale(scaleMatrix, scale);
	markRebuildTransform();
}

void Object::setScale(float x, float y, float z)
{
	setScale(glm::vec3(x, y, z));
}

void Object::setScale(float scalar)
{
	setScale(glm::vec3(scalar, scalar, scalar));
}

glm::mat4 Object::getTransformMatrix()
{
	if (requiresTransformRebuild)
	{
		requiresTransformRebuild = false;
		glm::mat4 parentMat = glm::mat4();
		if (parent != nullptr)
		{
			parentMat = parent->getTransformMatrix();
		}

		glm::mat4 rotMat = glm::mat4_cast(rotationQuat);
		transformMatrix = parentMat * positionMatrix * rotMat * scaleMatrix; // parentMat * 
	}
	return transformMatrix;
}

glm::vec3 Object::forward()
{
	// Quaternion vectors from: https://www.gamedev.net/forums/topic/56471-extracting-direction-vectors-from-quaternion/
	float x = 2 * (rotationQuat.x * rotationQuat.z + rotationQuat.w * rotationQuat.y);
	float y = 2 * (rotationQuat.y * rotationQuat.z - rotationQuat.w * rotationQuat.x);
	float z = 1 - 2 * (rotationQuat.x * rotationQuat.x + rotationQuat.y * rotationQuat.y);
	return glm::normalize(glm::vec3(x, y, z));
}

glm::vec3 Object::left()
{
	float x = 1 - 2 * (rotationQuat.y * rotationQuat.y + rotationQuat.z * rotationQuat.z);
	float y = 2 * (rotationQuat.x * rotationQuat.y + rotationQuat.w * rotationQuat.z);
	float z = 2 * (rotationQuat.x * rotationQuat.z - rotationQuat.w * rotationQuat.y);
	return glm::normalize(glm::vec3(x, y, z));
}

glm::vec3 Object::up()
{
	float x = 2 * (rotationQuat.x * rotationQuat.y - rotationQuat.w * rotationQuat.z);
	float y = 1 - 2 * (rotationQuat.x * rotationQuat.x + rotationQuat.z * rotationQuat.z);
	float z = 2 * (rotationQuat.y * rotationQuat.z + rotationQuat.w * rotationQuat.x);
	return glm::normalize(glm::vec3(x, y, z));
}

void Object::destroy()
{
	destroyed = true;
	for (int i = 0; i < children.size(); i++)
	{
		children[i]->destroy();
	}
}

void Object::markRebuildTransform()
{
	requiresTransformRebuild = true;
	for (int i = 0; i < children.size(); i++)
	{
		children[i]->markRebuildTransform();
	}
}

glm::quat Object::lookAt(glm::vec3 target)
{
	return Object::lookAt(getPosition(), target);
}

glm::vec3 Object::lookAtEuler(glm::vec3 target)
{
	return Object::lookAtEuler(getPosition(), target);
}

glm::quat Object::lookAt(glm::vec3 pos, glm::vec3 target)
{
	if (pos == target)
		target = pos + glm::vec3(1, 0, 0);

	glm::mat4 mat = glm::lookAt(pos, target, glm::vec3(0, 1, 0));
	glm::quat quat = glm::quat_cast(mat);
	return glm::rotate(glm::inverse(quat), 180.0f, glm::vec3(0, 1, 0));
}

glm::vec3 Object::lookAtEuler(glm::vec3 pos, glm::vec3 target)
{
	return glm::eulerAngles(Object::lookAt(pos, target));
}
