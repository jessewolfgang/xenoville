#pragma once

#include <stdio.h>  
#include <stdlib.h> 
#include <vector>

#include <GL/glew.h>  

#include <glm/glm.hpp>
#include <glm/common.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>


/// <summary>
/// Represents a single object with a position, rotation and scale.
/// </summary>
class Object
{
public:	
	/// <summary>
	/// Degrees to Radians constant.
	/// </summary>
	//static const float DEG2RAD;
		
	/// <summary>
	/// Radians to Degrees constant.
	/// </summary>
	//static const float RAD2DEG;

	/// <summary>
	/// Whether the object has been destroyed.
	/// </summary>
	bool destroyed;
	
	/// <summary>
	/// Whether the transform matrix should be rebuild on the next access.
	/// </summary>
	bool requiresTransformRebuild;

	Object();
	~Object();

	Object* parent;

	/// <summary>
	/// Sets a child object
	/// </summary>
	void setChild(Object* child);

	/// <summary>
	/// Gets the position of the object.
	/// </summary>
	/// <returns>The position vector.</returns>
	glm::vec3 getPosition();

	/// <summary>
	/// Sets the position of the object.
	/// </summary>
	/// <param name="pos">The position vector.</param>
	void setPosition(glm::vec3 pos);

	/// <summary>
	/// Sets the position of the object.
	/// </summary>
	/// <param name="x">X pos.</param>
	/// <param name="y">Y pos.</param>
	/// <param name="z">Z pos.</param>
	void setPosition(float x, float y, float z);

	/// <summary>
	/// Gets the rotation of the object in euler angles.
	/// </summary>
	/// <returns>The rotation in a euler angles vector.</returns>
	glm::vec3 getRotationEuler();
	
	/// <summary>
	/// Gets the rotation of the object as a quaternion.
	/// </summary>
	/// <returns>The rotation in a quaternion.</returns>
	glm::quat getRotation();
		
	/// <summary>
	/// Sets the rotation of the object.
	/// </summary>
	/// <param name="rot">The rotation euler angles vector.</param>
	void setRotationEuler(glm::vec3 rot);
	
	/// <summary>
	/// Sets the rotation.
	/// </summary>
	/// <param name="x">The x.</param>
	/// <param name="y">The y.</param>
	/// <param name="z">The z.</param>
	void setRotationEuler(double x, double y, double z);

	/// <summary>
	/// Sets the rotation of the object.
	/// </summary>
	/// <param name="quat">The quaternion.</param>
	void setRotation(glm::quat quat);

	/// <summary>
	/// Gets the scale of the object.
	/// </summary>
	/// <returns>The scale as a vector.</returns>
	glm::vec3 getScale();
	
	/// <summary>
	/// Sets the scale of the object.
	/// </summary>
	/// <param name="scale">The scale vector.</param>
	void setScale(glm::vec3 scale);
	
	/// <summary>
	/// Sets the scale of the object.
	/// </summary>
	/// <param name="x">The X scale.</param>
	/// <param name="y">The Y scale.</param>
	/// <param name="z">The Z scale.</param>
	void setScale(float x, float y, float z);
	
	/// <summary>
	/// Sets the scale of the object by all axes.
	/// </summary>
	/// <param name="scalar">The scale to set each axis to.</param>
	void setScale(float scalar);

	/// <summary>
	/// Rotates the object.
	/// </summary>
	/// <param name="rotation">The rotation.</param>
	void rotate(glm::vec3 rotation);
	
	/// <summary>
	/// Rotates the object around local axes.
	/// </summary>
	/// <param name="rotation">The local rotation.</param>
	void rotateLocal(glm::vec3 rotation);
	
	/// <summary>
	/// Translates the object.
	/// </summary>
	/// <param name="translation">The translation.</param>
	void translate(glm::vec3 translation);

	/// <summary>
	/// Gets the transform matrix from the object state.
	/// </summary>
	/// <returns>A 4x4 matrix represeting the transform.</returns>
	glm::mat4 getTransformMatrix();
	
	/// <summary>
	/// Gets the forward vector.
	/// </summary>
	/// <returns>A vector pointing forward.</returns>
	glm::vec3 forward();
	
	/// <summary>
	/// Gets the left vector.
	/// </summary>
	/// <returns>A vectpr pointing left.</returns>
	glm::vec3 left();
	
	/// <summary>
	/// Gets the up vector.
	/// </summary>
	/// <returns>A vectpr pointing up.</returns>
	glm::vec3 up();
	
	/// <summary>
	/// Queues the object to be destroyed at the end of the frame.
	/// </summary>
	void destroy();
	
	/// <summary>
	/// Marks the object and its children that their transforms need to be rebuilt.
	/// </summary>
	void markRebuildTransform();
	
	/// <summary>
	/// Creates a rotation from this object's position looking at a given point.
	/// </summary>
	/// <param name="target">The target.</param>
	/// <returns>An quaternion.</returns>
	glm::quat lookAt(glm::vec3  target);

	/// <summary>
	/// Creates a rotation from this object's position looking at a given point.
	/// </summary>
	/// <param name="target">The target.</param>
	/// <returns>An euler rotation vector.</returns>
	glm::vec3 lookAtEuler(glm::vec3 target);

	/// <summary>
	/// Creates a rotation from a position looking at a given point.
	/// </summary>
	/// <param name="pos">The position.</param>
	/// <param name="target">The target.</param>
	/// <returns>A quaternion.</returns>
	static glm::quat lookAt(glm::vec3 pos, glm::vec3 target);

	/// <summary>
	/// Creates a rotation from a position looking at a given point.
	/// </summary>
	/// <param name="pos">The position.</param>
	/// <param name="target">The target.</param>
	/// <returns>An euler rotation vector.</returns>
	static glm::vec3 lookAtEuler(glm::vec3 pos, glm::vec3 target);

protected:
	//glm::mat4 transform;
	//glm::vec3 position;
	//glm::vec3 rotation;
	//glm::vec3 scale;
	
	glm::mat4 transformMatrix;

	std::vector<Object*> children;

	glm::quat rotationQuat;

	glm::mat4 positionMatrix;
	//glm::mat4 rotationMatrix;
	glm::mat4 scaleMatrix;
};

