#include "Plant.h"

std::vector<MaterialStandard*> Plant::materials;
std::vector<MaterialStandard*> Plant::specialMaterials;

Plant::Plant()
{

}

Plant::~Plant()
{
}

void Plant::generateMaterials()
{
	MaterialStandard* metal = new MaterialStandard();
	metal->tintColour = glm::vec3(0, 0, 0);
	metal->specular = glm::vec3(6, 6, 6);
	metal->shininess = 1001; //Its over 1000!!!
	metal->reflectionColour = glm::vec3(1, 1, 1);
	metal->fresnelExponent = 0;
	materials.push_back(metal);

	MaterialStandard* wood = new MaterialStandard();
	wood->diffuseMap = Engine::loadTexture("Wood");
	wood->normalMap = Engine::loadTexture("WoodNormals");
	wood->specularMap = Engine::loadTexture("WoodSpecular");
	materials.push_back(wood);

	MaterialStandard* bark = new MaterialStandard();
	bark->diffuseMap = Engine::loadTexture("Bark", Engine::Albedo);
	bark->normalMap = Engine::loadTexture("BarkNormals", Engine::Normal);
	bark->specularMap = Engine::loadTexture("BarkSpecular", Engine::Specular);
	bark->tintColour = glm::vec3(1.0f, 1.0f, 1.0f);
	bark->texScale = glm::vec2(4, 1);
	materials.push_back(bark);

	MaterialStandard* leaves = new MaterialStandard();
	leaves->diffuseMap = Engine::loadTexture("GrassHDDiffuse", Engine::Albedo);
	leaves->normalMap = Engine::loadTexture("GrassHDNormals", Engine::Normal);
	leaves->specularMap = Engine::loadTexture("GrassHDSpecular", Engine::Specular);
	leaves->shininess = 40;
	leaves->specular = glm::vec3(5);
	materials.push_back(leaves);

	MaterialStandard* emission = new MaterialStandard();
	emission = new MaterialStandard();
	emission->emission = glm::vec3(4, 4, 4);
	materials.push_back(emission);

	MaterialStandard* crystal = new MaterialStandard();
	crystal->diffuseMap = Engine::loadTexture("Plasma", Engine::Albedo);
	crystal->normalMap = Engine::loadTexture("dirtNormals", Engine::Normal);
	crystal->tintColour = glm::vec3(0.8f, 0, 0);
	crystal->shininess = 50;
	crystal->specular = glm::vec3(4);
	crystal->normalMapMultiplier = 0.2f;
	crystal->reflectionColour = glm::vec3(1);
	crystal->fresnelExponent = 2.0f;
	materials.push_back(crystal);

	MaterialStandard* slimy = new MaterialStandard();
	slimy->normalMap = Engine::loadTexture("CloudNormal", Engine::Normal);
	slimy->normalMapMultiplier = 0.2f;
	slimy->texScale = glm::vec2(2, 2);
	slimy->tintColour = glm::vec3(0, 1.0f, 0.3f) * 0.3f;
	slimy->specular = glm::vec3(6, 6, 6);
	slimy->shininess = 300;
	slimy->alpha = 0.7f;
	slimy->reflectionColour = glm::vec3(1);
	slimy->fresnelExponent = 1;
	slimy->setTransparent();
	materials.push_back(slimy);

	MaterialStandard* ghostly = new MaterialStandard();
	ghostly->shininess = 400;
	ghostly->reflectionColour = glm::vec3(1);
	ghostly->fresnelExponent = 0.3f;
	ghostly->setTransparent();
	ghostly->alpha = 0.1f;
	materials.push_back(ghostly);

	MaterialStandard* kitten = new MaterialStandard();
	kitten->diffuseMap = Engine::loadTexture("Kitten", Engine::Albedo);
	kitten->tintColour = glm::vec3(1.0f, 1.0f, 1.0f);
	kitten->texScale = glm::vec2(2, 2);
	kitten->shininess = 200;
	materials.push_back(kitten);

	MaterialStandard* stone = new MaterialStandard();
	stone->diffuseMap = Engine::loadTexture("MetalDiffuse", Engine::Albedo);
	stone->normalMap = Engine::loadTexture("MetalNormals", Engine::Normal);
	stone->specularMap = Engine::loadTexture("MetalSpecular", Engine::Specular);
	stone->specular = glm::vec3(4, 4, 4);
	stone->shininess = 60;
	stone->normalMapMultiplier = 0.1f;
	stone->texScale = glm::vec2(1, 1);
	stone->reflectionColour = glm::vec3(1);
	materials.push_back(stone);


	
	// ----- SPECIAL MATERIALS -----
	MaterialStandard* eyeLeaf = new MaterialStandard();
	eyeLeaf->diffuseMap = Engine::loadTexture("Eyeball", Engine::Albedo);
	eyeLeaf->specular = glm::vec3(2, 2, 2);
	eyeLeaf->shininess = 300;
	eyeLeaf->reflectionColour = glm::vec3(2);
	eyeLeaf->fresnelExponent = 2;
	specialMaterials.push_back(eyeLeaf);

}

void Plant::destroy()
{
	stem->destroy();
	for (ModelObject* l : leaves)
	{
		l->destroy();
	}
}

void Plant::BuildPlant(int stemId, int leafId, Size sca, Appearance stemApp, Appearance leafApp, glm::vec3 position)
{
	stemTexture = materials[stemApp];
	scale = sca;

	stem = new ModelObject();
	stem->setPosition(position);
	float scaleFloat = 0.15f;
	switch (scale)
	{
	case Tiny: scaleFloat *= 0.5f;
		break;
	case Small: scaleFloat *= 0.7f;
		break;
	case Average: scaleFloat *= 1.0f;
		break;
	case Large: scaleFloat *= 1.2f;
		break;
	case AbsoluteUnit: scaleFloat *= 3.0f;
		break;
	}

	stem->setScale(scaleFloat);
	stem->loadMesh("Stem" + std::to_string(stemId));

	// Fill materials.
	int stemObjCount = stem->getMesh()->asset->parts.size();
	for (int i = 0; i < stemObjCount; i++)
	{
		stem->setMaterial(i, stemTexture);
	}
	
	leafTexture = materials[leafApp];

	int minLeaves = 30;
	int maxLeaves = 100;
	int leafCount = minLeaves + rand() % (1 + maxLeaves - minLeaves);
	
	// Attach a bunch of leaves to random vertices of the mesh.
	for (int i = 0; i < leafCount; i++)
	{
		int randMeshPart = rand() % stemObjCount;
		MeshPart* stemPart = stem->getMesh()->asset->parts[randMeshPart];

		int randomIndex = rand() % stemPart->vertexCount;
		Vertex v = stemPart->vertices[randomIndex];

		ModelObject* leafModel = new ModelObject(); 
		stem->setChild(leafModel);

		leafModel->setPosition(v.position);
		leafModel->setRotationEuler(Object::lookAtEuler(glm::vec3(0), -v.normal));
		leafModel->setScale(Game::nextRange(0.3f, 0.6f));
		leafModel->loadMesh("Leaf" + std::to_string(leafId));
		leaves.push_back(leafModel);

		int leafPartCount = leafModel->getMesh()->asset->parts.size();
		for (int j = 0; j < leafPartCount; j++)
		{
			leafModel->setMaterial(j, leafTexture);
		}
		applySpecialMaterialProperties(leafModel);
	}
}

void Plant::applySpecialMaterialProperties(ModelObject* model)
{
	if (model->getMesh()->asset->assetName == "Leaf1")
	{
		model->setMaterial(1, specialMaterials[0]);
	}
}

