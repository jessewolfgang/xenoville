#pragma once
#include <string>
#include "TextureAsset.h"
#include "MaterialStandard.h"
#include "ModelObject.h"
#include "Engine.h"

/// <summary>
/// The root of a plant structure.
/// </summary>
class Plant
{
public:
	
	// Dont put anything after the ENUM_COUNT consts.
	enum Size {Tiny, Small, Average, Large, AbsoluteUnit, ENUM_SIZE_COUNT};
	enum Appearance {Metal, Wood, Bark, Leaves, Emissive, Crystal, Slimy, Ghostly, Cat, Stone, ENUM_APPEARANCE_COUNT};

	//Metal - Reflection, Wood - Normals/Fresnal, Emissive - HDR/Bloom, Crystal - Hue, Slimy, Ghostly - Transperancy, Cat - Memes, Stone
	
	/// <summary>
	/// The species name.
	/// </summary>
	std::string speciesName;

	Plant();
	~Plant();

	static std::vector<MaterialStandard*> materials;
	static std::vector<MaterialStandard*> specialMaterials;
	static void generateMaterials();
	
	//Attributes
	int StemModelId;	 //Model of the Stem
	int LeafModelId;	 //Model of the Leaf
	Size scale;			 //Size (scale)
	Appearance stemApperance;  //Stem Material
	Appearance leafApperance;  //Leaf Material

	void destroy();

	void BuildPlant(int stemId, int leafId, Size sca, Appearance stemApp, Appearance leafApp, glm::vec3 position);

protected:
	ModelObject* stem;
	std::vector<ModelObject*> leaves;
	std::vector<glm::vec3> leafPositions;
	int size;

	MaterialStandard* stemTexture;
	MaterialStandard* leafTexture;

	void applySpecialMaterialProperties(ModelObject* model);


};

