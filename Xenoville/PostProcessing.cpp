#include "PostProcessing.h"
#include "Engine.h"


PostProcessing::PostProcessing()
{
	bloomIntensity = 2.6f;
	bloomContrast = 0.2f;
	bloomRamp = 4.5f;
	bloomColour = glm::vec3(0.15f, 0.22f, 1.1f);
}


PostProcessing::~PostProcessing()
{
}

void PostProcessing::applyProperties(GLuint postShader)
{
	glUniform1f(glGetUniformLocation(postShader, "bloomIntensity"), bloomIntensity);
	glUniform1f(glGetUniformLocation(postShader, "bloomContrast"), bloomContrast);
	glUniform1f(glGetUniformLocation(postShader, "bloomRamp"), bloomRamp);
	glUniform3fv(glGetUniformLocation(postShader, "bloomColour"), 1, glm::value_ptr(bloomColour));

}
