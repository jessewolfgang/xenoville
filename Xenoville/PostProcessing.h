#pragma once
#include <GL/glew.h>
#include <glm.hpp>

/// <summary>
/// Used for holding values for the post processing effects.
/// </summary>
class PostProcessing
{
public:
	
	/// <summary>
	/// The intensity of the bloom.
	/// </summary>
	float bloomIntensity;
	
	/// <summary>
	/// The bloom contrast.
	/// </summary>
	float bloomContrast;
	
	/// <summary>
	/// The bloom ramp.
	/// </summary>
	float bloomRamp;
	
	/// <summary>
	/// The bloom colour.
	/// </summary>
	glm::vec3 bloomColour;

	PostProcessing();
	~PostProcessing();
		
	/// <summary>
	/// Applies the post processing properties.
	/// </summary>
	void applyProperties(GLuint postShader);
};

