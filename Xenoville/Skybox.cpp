#include "Skybox.h"
#include <GL/glew.h>
#include "Engine.h"
#include "Cubemap.h"

Skybox::Skybox()
{
	skyCube = 0;
	skyReflectionCube = 0;
	exposure = 1;
	tintColour = glm::vec3(1);

	/*skyMaterial = new MaterialUnlit();
	skyMaterial->colourMap = skyTexture; //
	skyMaterial->colour = glm::vec3(3.0f, 3.0f, 3.0f);
	skyMaterial->exposure = 0.5f;
	objectSkySphere = new ModelObject();
	objectSkySphere->setPosition(Engine::getCamera()->getPosition());
	objectSkySphere->setScale(1000);
	objectSkySphere->loadMesh("SkySphere", skyMaterial);*/
}

Skybox::~Skybox()
{
}

void Skybox::render()
{
	glDepthMask(GL_FALSE);
	//skyMaterial->colourMap = skyTexture;
	//skyMaterial->exposure = exposure;
	//objectSkySphere->setPosition(Engine::getCamera()->getPosition());

	GLuint shaderProgram = Engine::getShaderSkybox();
	glUseProgram(shaderProgram);

	// Vert vars
	GLint uniView = glGetUniformLocation(shaderProgram, "view");
	glm::mat4 view = glm::mat4(glm::mat3(Engine::getCamera()->getViewMatrix()));
	glm::mat4 viewMat = Engine::getCamera()->getViewMatrix();
	glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));

	GLint uniProj = glGetUniformLocation(shaderProgram, "proj");
	glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(Engine::getCamera()->getProjectionMatrix()));

	// Frag vars 
	glUniform3fv(glGetUniformLocation(shaderProgram, "colourTint"), 1, glm::value_ptr(tintColour));
	glUniform1f(glGetUniformLocation(shaderProgram, "exposure"), exposure);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skyCube);

	Cubemap::drawCube();

	glDepthMask(GL_TRUE);
}

