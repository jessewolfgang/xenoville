#pragma once
#include <glm.hpp>
#include "TextureAsset.h"
#include "ModelObject.h"
/// <summary>
/// A skybox (or sphere) that is rendered around a scene.
/// </summary>
class Skybox
{
public:
	
	/// <summary>
	/// The sky cube.
	/// </summary>
	GLuint skyCube;
	
	/// <summary>
	/// The cube texture used for sampling sky reflections. Typically lower resolution.
	/// </summary>
	GLuint skyReflectionCube;
	
	/// <summary>
	/// The sky exposure.
	/// </summary>
	float exposure;
	
	/// <summary>
	/// The tint colour.
	/// </summary>
	glm::vec3 tintColour;

	Skybox();
	~Skybox();
	
	/// <summary>
	/// Updates the skybox.
	/// </summary>
	void render();

};

