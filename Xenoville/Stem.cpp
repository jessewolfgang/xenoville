#include "Stem.h"

Stem::Stem()
{

}

Stem::~Stem()
{

}

glm::vec3 Stem::getLeafPosition(int i)
{
	return leafPositions[i];
}

void Stem::setLeafPosition(int i, glm::vec3 pos)
{
	this->leafPositions[i] = pos;
}

Leaf* Stem::getLeafType()
{
	return leafType;
}

void Stem::setLeafType(Leaf* leaf)
{
	this->leafType = leaf;
}
