#pragma once
#include <vector>
#include <glm.hpp>
#include "Leaf.h"

/// <summary>
/// The base extrusion of a plant.
/// </summary>
class Stem
{
public:
	Stem();
	~Stem();

	Leaf* getLeafType();
	void setLeafType(Leaf* leaf);

	glm::vec3 getLeafPosition(int i);
	void setLeafPosition(int i, glm::vec3 pos);

protected:
	Leaf* leafType;
	
	//glm::vec3 leafPositions [10];
	std::vector<glm::vec3> leafPositions;


};
