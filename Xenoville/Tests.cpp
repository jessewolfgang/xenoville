#include "Tests.h"
#include "Object.h"

Tests::Tests()
{
}


Tests::~Tests()
{
}

void Tests::runTests()
{
	Object obj = Object();

	obj.setPosition(1, 69, 420);
	glm::vec3 pos = obj.getPosition();
	std::cout << "Pos: " << pos.x << ", " << pos.y << ", " << pos.z << std::endl;

	obj.setRotationEuler(1, 69, 420);
	glm::vec3 rot = obj.getRotationEuler();
	std::cout << "Rot: " << rot.x << ", " << rot.y << ", " << rot.z << std::endl;

	obj.setScale(1, 69, 420);
	glm::vec3 scl = obj.getScale();
	std::cout << "Scl: " << scl.x << ", " << scl.y << ", " << scl.z << std::endl;

	std::cin.get();
}
