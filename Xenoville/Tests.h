#pragma once

/// <summary>
/// A class for running tests.
/// </summary>
class Tests
{
public:
	Tests();
	~Tests();
	
	/// <summary>
	/// Runs the tests.
	/// </summary>
	void runTests();
};

