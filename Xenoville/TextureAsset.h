#pragma once
#include <string>
#include <GL/glew.h>

/// <summary>
/// A 2D texture asset.
/// </summary>
struct TextureAsset
{
	/// <summary>
	/// The asset name of the image.
	/// </summary>
	std::string assetName;
	
	/// <summary>
	/// The GL texture identifier.
	/// </summary>
	GLuint glTextureId;
};