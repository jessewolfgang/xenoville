#pragma once
#include "glm.hpp"

/// <summary>
/// Vertex data structure.
/// </summary>
struct Vertex
{	
	/// <summary>
	/// Vertex position.
	/// </summary>
	glm::vec3 position;

	/// <summary>
	/// Vertex normal.
	/// </summary>
	glm::vec3 normal;

	/// <summary>
	/// Vertex colour.
	/// </summary>
	glm::vec4 colour;

	/// <summary>
	/// Vertex UV texture coordinates.
	/// </summary>
	glm::vec2 texCoords;
	
	/// <summary>
	/// The tangent.
	/// </summary>
	glm::vec3 tangent;
};