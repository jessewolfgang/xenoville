#include "Engine.h"
#include "Tests.h"

// Uncomment this line to run the testing class.
// #define RUNTESTS

int main(void)
{

#ifdef RUNTESTS
	Tests tests = Tests();
	tests.runTests();
#else
	Engine engine;
	return engine.run();
#endif

}